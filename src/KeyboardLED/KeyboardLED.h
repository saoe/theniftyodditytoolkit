/*!
    @file
    Utilities (classes, functions, etc.) related to making use of a keyboard's standard indicator LEDs (Windows-specific).
    
    Copyright © 2016, 2021-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::KeyboardLED
    @copybrief KeyboardLED.h

    @details
    - Header-only library
    - Uses the Win32 API!
    - Resource: "How to: Toggle the Num Lock, Caps Lock, and Scroll Lock keys" <https://support.microsoft.com/en-us/kb/127190>
    
    @note
    Some keyboards have a special layouts and/or features, for example my current keyboard,
    the Durgod Taurus K320 TKL (US-Layout): It has the normal CapsLock- and ScrollLock-LEDs,
    but misses the NumPad-LED (no need, since it's a TenKeyLess model without a number pad).
    Additionally it has two keyboard LEDs (in combination with Fn) for 'Windows Key Lock mode' and
    'MR mode' (Switch custom profile/Durgod Zeus Engine). I tried a while to find out if/how I could
    use these LEDs myself (OEM scan code, etc.), but after a while I gave up: Seems very custom,
    so it wouldn't make sense to support such a special model with my code. -- 2022-02-26.

    - 2016-01-10 (so): Created.
    - 2021-02-20 (so): General overhaul and refactoring.
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_28A6DB05_185C_4955_A1B9_5523453E2840
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_28A6DB05_185C_4955_A1B9_5523453E2840
 
#ifdef _WIN32

#include <bitset>
#include <vector>
#include <windows.h>

namespace NiftyOddity::Toolkit::KeyboardLED
{

//! @brief This is by design an unscoped enum: We want the implicit conversion from the enum key to its integer value!
enum ChangeStateTo
{
    // Binary-literal integers (0b...) are available since ISO C++ 2014.
    NumLockOff_CapsLockOff_SrcollLockOff = 0b000,
    NumLockOff_CapsLockOff_SrcollLockOn  = 0b001,
    NumLockOff_CapsLockOn_SrcollLockOff  = 0b010,
    NumLockOff_CapsLockOn_SrcollLockOn   = 0b011,
    NumLockOn_CapsLockOff_SrcollLockOff  = 0b100,
    NumLockOn_CapsLockOff_SrcollLockOn   = 0b101,
    NumLockOn_CapsLockOn_SrcollLockOff   = 0b110,
    NumLockOn_CapsLockOn_SrcollLockOn    = 0b111
};
        
using LightControl = std::bitset<3>;

/*!
    @brief   Holds a pattern that can be played/executed be used by class KeyboardLED.
    @details Outside of the class, so that multiple versions can be prepared and managed, regardless of the an object instance.
*/
struct Pattern
{
    unsigned int play_count = 1;
    unsigned int pause_between_patterns_in_milliseconds = 250;
    std::vector<LightControl> states;                          //!< Can be one of the predefined #ChangeStateTo enum values, or as an explicit bitset value.
};


/*!
    Utility class for making use of a keyboard's standard status LEDs.
*/
class KeyboardLED
{
    public:
        Pattern pattern;
    
        KeyboardLED ()
        {
            save_current_state();
        }
        
        KeyboardLED (Pattern p)
        {
            save_current_state();
            pattern = p;
        }

        ~KeyboardLED ()
        {
            restore_previous_state();
        }
        
        //! Restore the previous state (which was saved at the construction of this object).
        void reset ()
        {
            restore_previous_state();
        }

        /*!
            @brief Set the LED to an activated or deactivated state, depending on the parameter.
            @param should_light_up Whether the LED should light up (`true`) or not (`false`).
        */
        void setNumLock (bool should_light_up)
        {
            SHORT key_state = GetKeyState(VK_NUMLOCK);

            // If the LED is already in the desired state, do nothing; otherwise, trigger the change.
            if (!((key_state == 1 && should_light_up) || (key_state == 0 && !should_light_up)))
            {
                keybd_event(VK_NUMLOCK, 0x45, KEYEVENTF_EXTENDEDKEY | 0, 0);
                keybd_event(VK_NUMLOCK, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            }
        }

        //! @copydoc setNumLock()
        void setCapsLock (bool should_light_up)
        {
            SHORT key_state = GetKeyState(VK_CAPITAL);

            // If the LED is already in the desired state, do nothing; otherwise, trigger the change.
            if (!((key_state == 1 && should_light_up) || (key_state == 0 && !should_light_up)))
            {
                keybd_event(VK_CAPITAL, 0x45, KEYEVENTF_EXTENDEDKEY | 0, 0);
                keybd_event(VK_CAPITAL, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            }
        }

        //! @copydoc setNumLock()
        void setScrollLock (bool should_light_up)
        {
            SHORT key_state = GetKeyState(VK_SCROLL);

            // If the LED is already in the desired state, do nothing; otherwise, trigger the change.
            if (!((key_state == 1 && should_light_up) || (key_state == 0 && !should_light_up)))
            {
                keybd_event(VK_SCROLL, 0x45, KEYEVENTF_EXTENDEDKEY | 0, 0);
                keybd_event(VK_SCROLL, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
            }
        }

        /*!
            @brief Set all LEDs at once to an activated or deactivated state, depending on the parameters.
            @param should_light_up_numlock    Whether the LED should light up (`true`) or not (`false`).
            @param should_light_up_capslock   Whether the LED should light up (`true`) or not (`false`).
            @param should_light_up_scrolllock Whether the LED should light up (`true`) or not (`false`).
        */
        void setAll (bool should_light_up_numlock, bool should_light_up_capslock, bool should_light_up_scrolllock)
        {
            setNumLock(should_light_up_numlock);
            setCapsLock(should_light_up_capslock);
            setScrollLock(should_light_up_scrolllock);
        }

        //! @brief Invert the current state of all keyboard LEDs.
        void invert ()
        {
            setAll(!GetKeyState(VK_NUMLOCK), !GetKeyState(VK_CAPITAL), !GetKeyState(VK_SCROLL));
        }
        
        /*!
            @brief Flash all LEDs at once, according to the given parameter argument values.
            @param count                 How often the flashing should happen.
            @param pause_in_milliseconds How much time to wait between flashing.
        */
        void flash (unsigned int count = 1, unsigned int pause_in_milliseconds = 500)
        {
            for (unsigned int c = 0; c <= count; ++c)
            {
                setAll(true, true, true);
                Sleep(pause_in_milliseconds);
                setAll(false, false, false);
                Sleep(pause_in_milliseconds);
            }
            restore_previous_state();
        }

        /*!
            @brief Play the LightControl Pattern given by the parameter.
            @param p A LightControl Pattern
        */
        void play (struct Pattern p)
        {
            for (unsigned int pc = 1; pc <= p.play_count; ++pc)
            {
                for (auto iter = p.states.begin(); iter != p.states.end(); ++iter)
                {
                    setAll((*iter)[BitsetIndex::NumLock], (*iter)[BitsetIndex::CapsLock], (*iter)[BitsetIndex::ScrollLock]);
                    Sleep(p.pause_between_patterns_in_milliseconds);
                }
            }
            restore_previous_state();
        }

        //! @brief Play the LightControl Pattern of this object.
        void play ()
        {
            play(pattern);
        }

    private:
        /* Bitsets of 3 bits, with a fixed order; e.g. binary "101" means "(NumLock On)(CapsLock Off)(ScrollLock On)".
        
        Attention: If you want to access the bitset with an index (like an array), then keep in mind
                   that the order of it needs to be thought of as reverse!
                   Use the 'BitsetIndex::*Lock' enumeration for easier handling.
        
                   Value "1  0  1"
                   Index [2][1][0]
                          |  |  +-- Represents NumLock, the left LED on the actual keyboard.
                          |  +----- Represents CapsLock, the middle LED on the actual keyboard.
                          +-------- Represents ScrollLock, the right LED on the actual keyboard. */
        
        LightControl new_state;
        LightControl saved_state;
        
        // This is by design an unscoped enum: We want the implicit conversion from the enum key to its integer value!
        enum BitsetIndex
        {
            NumLock = 2,
            CapsLock = 1,
            ScrollLock = 0
        };
        
        void save_current_state (void)
        {
            saved_state[BitsetIndex::NumLock] = GetKeyState(VK_NUMLOCK);
            saved_state[BitsetIndex::CapsLock] = GetKeyState(VK_CAPITAL);
            saved_state[BitsetIndex::ScrollLock] = GetKeyState(VK_SCROLL);
        }
        
        void restore_previous_state (void)
        {
            setAll(saved_state[BitsetIndex::NumLock], saved_state[BitsetIndex::CapsLock], saved_state[BitsetIndex::ScrollLock]);
        }
};

} // namespace

#endif // _WIN32

#endif // include guard