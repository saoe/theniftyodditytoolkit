/*!
    @file
    Helper functions for working with files.
    
    Copyright © 2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::FileUtilities
    @copybrief FileUtilities.h
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_E49E0589_AEE2_4FF5_836A_DCD539208105
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_E49E0589_AEE2_4FF5_836A_DCD539208105

#if !defined(__cplusplus)
#error C++ compiler required
#endif

#include <iostream>
#include <filesystem>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <tuple>
#include <map>
#include <algorithm>
#include <cctype>

#if defined(_WIN32)
#include <windows.h>
#include <Wincrypt.h>
#include <msi.h>
#include <tchar.h>
#endif

#include "TextUtilities.h"
#include "CastConvert.h"

namespace tnottxt = NiftyOddity::Toolkit::TextUtilities;
namespace tnotcc = NiftyOddity::Toolkit::CastConvert;

namespace NiftyOddity::Toolkit::FileUtilities
{
    
enum class HashAlgorithm
{
    MD5,
    SHA1,
	SHA256,
    SHA384,
	SHA512
};

struct FileVersionInfo
{
    // SemVer uses 3 components: Major.Minor.Patch; Win32 uses 4 components: Major.Minor.Build.Revision. This here is mix of both.
    unsigned int FileMajorVersion;
    unsigned int FileMinorVersion;
    unsigned int FilePatchVersion;
    unsigned int FileRevisionVersion;
    
    // FileVersion and ProductVersion are often set to the same value, but there are cases where the may differ,
    // e.g. when a file is just a part of a collection that represents the actual product.
    // Also, for a MSI, we currently set only the ProductVersion.
    unsigned int ProductMajorVersion;
    unsigned int ProductMinorVersion;
    unsigned int ProductPatchVersion;
    unsigned int ProductRevisionVersion;
};

#if defined(_WIN32)
inline namespace Windows
{

/*!
    Calculate the hashing checksum of a file.

    @param filepath The path to the file for which the checksum should be calculated.
    @param algo     The hashing algorithm that should be used for the calculation; the supported one are listed in HashAlgorithm.

    @return A std::string of hexadecimal digits that represents the checksum.

    @throw std::runtime_error with some details if one of the steps fails.
*/
std::string calculateFileChecksum (std::filesystem::path filepath, HashAlgorithm algo)
{
    std::error_code ec;
    std::stringstream exception_message;
    
    HCRYPTPROV csp = 0; // The cryptographic service provider (CSP) that should be used.
    unsigned int hash_algorithm = 0;

    switch (algo)
    {
        case HashAlgorithm::MD5:
            csp = PROV_RSA_AES; // [WIN32] For hashing, PROV_RSA_AES supports all algorithms that are currently used here: MD2, MD4, MD5, SHA-1, SHA-2 (SHA-256, SHA-384, and SHA-512).
            hash_algorithm = CALG_MD5;
            break;
        
        case HashAlgorithm::SHA1:
            csp = PROV_RSA_AES;
            hash_algorithm = CALG_SHA1;
            break;
        
        case HashAlgorithm::SHA256:
            csp = PROV_RSA_AES;
            hash_algorithm = CALG_SHA_256;
            break;
        
        case HashAlgorithm::SHA384:
            csp = PROV_RSA_AES;
            hash_algorithm = CALG_SHA_384;
            break;
        
        case HashAlgorithm::SHA512:
            csp = PROV_RSA_AES;
            hash_algorithm = CALG_SHA_512;
            break;
        
        default:
            exception_message << "unknown algorithm";
            break;
    }

    if (std::filesystem::exists(filepath) && std::filesystem::is_regular_file(filepath, ec))
    {
        // Read all bytes of the input file as the data for which the checksum should be calculated:
        std::ifstream infile(filepath.native().c_str(), std::ios::binary);

        infile.seekg (0, infile.end);
        size_t file_length = infile.tellg();
        infile.seekg (0, infile.beg);

        std::vector<unsigned char> file_data;
        file_data.resize(file_length);
        
        infile.read((char*)file_data.data(), file_data.size());
        infile.close();
        
        // Now calculate the checksum for that data:
        HCRYPTPROV provider = 0; // Handle to the cryptographic service provider (CSP).
        HCRYPTHASH hash_obj = 0; // Handle to hash object.
        
        if (!CryptAcquireContext(&provider, NULL, NULL, csp, CRYPT_VERIFYCONTEXT))
        {
            exception_message << "CryptAcquireContext failed: 0x" << std::hex << GetLastError();
        }

        if (!CryptCreateHash(provider, hash_algorithm, 0, 0, &hash_obj))
        {
            exception_message << "CryptCreateHash failed: 0x" << std::hex << GetLastError();
            if (provider) { CryptReleaseContext(provider, 0); }
        }

        if (!CryptHashData(hash_obj, file_data.data(), file_length, 0))
        {
            exception_message << "CryptHashData failed: 0x" << std::hex << GetLastError();
            if (provider) { CryptReleaseContext(provider, 0); }
            if (hash_obj) { CryptDestroyHash(hash_obj); }
        }

        DWORD dwCount = sizeof(DWORD);
        DWORD hash_byte_count = 0; // number of bytes in the hash value
        
        if(!CryptGetHashParam(hash_obj, HP_HASHSIZE, (BYTE*) &hash_byte_count, &dwCount, 0))
        {
            exception_message << "CryptGetHashParam/HP_HASHSIZE failed: 0x" << std::hex << GetLastError();
            if (provider) { CryptReleaseContext(provider, 0); }
            if (hash_obj) { CryptDestroyHash(hash_obj); }
        }
        
        std::vector<unsigned char> hash_byte_value;
        hash_byte_value.resize(hash_byte_count);

        DWORD cbHash = hash_byte_count;
        char str[1024]; //< @FIXME random size...
        DWORD strcount = _countof(str);
        
        if (CryptGetHashParam(hash_obj, HP_HASHVAL, hash_byte_value.data(), &cbHash, 0))
        {
            CryptBinaryToStringA(hash_byte_value.data(), cbHash, CRYPT_STRING_HEXRAW, str, &strcount);
            
            if (hash_obj)
                CryptDestroyHash(hash_obj);
    
            if (provider)
                CryptReleaseContext(provider, 0);
            
            return std::string(str);
        }
        else
        {
            exception_message << "CryptGetHashParam/HP_HASHVAL failed: 0x" << std::hex << GetLastError();
            if (provider) { CryptReleaseContext(provider, 0); }
            if (hash_obj) { CryptDestroyHash(hash_obj); }
            throw std::runtime_error(exception_message.str());
        }
    }
    else
    {
        exception_message << filepath << " is not a valid filepath";
        throw std::runtime_error(exception_message.str());
    }
}


/*!
    Extract information about the file and product version from a binary file.

    This handles binary files with an extension of EXE, DLL or MSI.

    @param path The path to the file from which the information should be read.

    @return FileVersionInfo struct.
*/
FileVersionInfo getFileVersionInfo (std::filesystem::path path)
{
    std::error_code ec;

    FileVersionInfo fvi = {};

    if (std::filesystem::exists(path) && std::filesystem::is_regular_file(path, ec))
    {
        std::string fnext = path.extension().string();
        std::transform(fnext.begin(), fnext.end(), fnext.begin(), [](unsigned char c) { return std::tolower(c); });
        
        if (fnext == ".exe" | fnext == ".dll")
        {
            DWORD dummy;
	        DWORD size = ::GetFileVersionInfoSize(path.native().c_str(), &dummy);
        
            LPBYTE VersionInfoData = new BYTE[size];
            ::GetFileVersionInfo(path.native().c_str(), 0, size, VersionInfoData);

            UINT len;
	        VS_FIXEDFILEINFO* ffi = nullptr;

            ::VerQueryValue(VersionInfoData, _T("\\"), (LPVOID*)&ffi, &len);
                // The second argument (_T("\\")) is the sub-block of the version info that should be retrieved:
                //   "The string must consist of names separated by backslashes (\) and it must have one of the following forms:
                //   "\ = The root block" [...]
	
            fvi.FileMajorVersion    = HIWORD(ffi->dwFileVersionMS);
	        fvi.FileMinorVersion    = LOWORD(ffi->dwFileVersionMS);
	        fvi.FilePatchVersion    = HIWORD(ffi->dwFileVersionLS);
	        fvi.FileRevisionVersion = LOWORD(ffi->dwFileVersionLS);

            fvi.ProductMajorVersion    = HIWORD(ffi->dwProductVersionMS);
	        fvi.ProductMinorVersion    = LOWORD(ffi->dwProductVersionMS);
	        fvi.ProductPatchVersion    = HIWORD(ffi->dwProductVersionLS);
	        fvi.ProductRevisionVersion = LOWORD(ffi->dwProductVersionLS);

	        delete[] VersionInfoData;
        }
        else if (fnext == ".msi")
        {
            TCHAR buffer[64] = { 0 };
            DWORD buffer_size = sizeof(buffer) / sizeof(buffer[0]);

            CoInitialize(NULL);
            MsiSetInternalUI(INSTALLUILEVEL_NONE, NULL);
                /*
                    2022-04-06 (so): When opening a random MSI for testing, a window briefly popped up and disappeard again a second later;
                    found an explanation at <https://microsoft.public.platformsdk.msi.narkive.com/RDFpRMiT/can-you-call-msiopenpackage-silently>:
                    
                    [?] Can you call MsiOpenPackage silently? 
                    > Actually, creating an installation engine typically results in the creation of the "Preparing to install" dialog.
                    > It's what you initially see when you start an installation with basic UI.
                    > The MsiOpenPackage API unlike MsiOpenDatabase creates an install engine that can allow you to drive an installation [...].
                    > 
                    > Precede your call to MsiOpenPackage with a call to MsiSetInternalUI using the INSTALLUILEVEL_NONE setting to turn off the dialog.
                    ---Carolyn Napier (Microsoft Windows Installer Team), 2003-09-22.
                */

            MSIHANDLE handle = NULL;
            UINT retval = MsiOpenPackage(path.native().c_str(), &handle);
            //if (retval != ERROR_SUCCESS)
            //{
            //    return retval;
            //}

            MsiGetProductProperty(handle, _T("ProductVersion"), buffer, &buffer_size);
                // [i] Synopsis: MsiGetFileVersion is not the correct function to check the version of a MSI; use MsiGetProductProperty.
                //     <https://stackoverflow.com/questions/815744/retrieving-version-of-an-msi-file-built-with-wix>
            MsiCloseHandle(handle);

            std::string x (tnotcc::narrowString(buffer).c_str());
            auto ss = tnottxt::splitString(x, '.');

            fvi.ProductMajorVersion    = std::stoi(ss.at(0));
	        fvi.ProductMinorVersion    = std::stoi(ss.at(1));
	        fvi.ProductPatchVersion    = std::stoi(ss.at(2));
	        fvi.ProductRevisionVersion = std::stoi(ss.at(3));
        }
    }

    return fvi;
}

} // namespace Windows
#endif

} // namespace

#endif // include guard