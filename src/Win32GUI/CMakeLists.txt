# 
# Win32GUI
# 
# Copyright © 2021-2022 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT-0
# Part of "The Nifty Oddity Toolkit (TNOT)"
# 

get_filename_component (CURRENT_DIRECTORY_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project (${CURRENT_DIRECTORY_NAME})

set (FULLY_QUALIFIED_PROJECT_NAME_STRING "${CMAKE_PROJECT_NAME}${PROJECT_NAME}")
    # To give the config/export files an unique name.
    # Note: Don't use any seperator (like '-' or '_'), to simplify the mapping of filenames and component namespaces.

# Header-/Interface-only library: No compilation will be done.
add_library (${PROJECT_NAME} INTERFACE
    Win32GUI.h
)

# Namespaced alias (for find_package())
add_library (${CMAKE_PROJECT_NAME}::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

# Will be transitively forwarded to consumers of this target.
target_include_directories (${PROJECT_NAME} INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
    $<INSTALL_INTERFACE:${TNOT_INSTALL_INCLUDEDIR}>
)

# --------------------------------------------------------------------------------------------------
# Installation Rules and Preparation for find_package()

# --- Basic Installation Rules ---

install (
    FILES
        Win32GUI.h
	DESTINATION ${TNOT_INSTALL_INCLUDEDIR}
)

install (
    TARGETS ${PROJECT_NAME}
    EXPORT  ${FULLY_QUALIFIED_PROJECT_NAME_STRING}Targets
)

# --- Export Targets ---

install (
    EXPORT      ${FULLY_QUALIFIED_PROJECT_NAME_STRING}Targets
    FILE        ${FULLY_QUALIFIED_PROJECT_NAME_STRING}Targets.cmake
    NAMESPACE   ${CMAKE_PROJECT_NAME}::
    COMPONENT   ${PROJECT_NAME}
    DESTINATION ${TNOT_INSTALL_CMAKEDIR}
)

export (
    TARGETS   ${PROJECT_NAME}
    NAMESPACE ${CMAKE_PROJECT_NAME}::
    FILE      ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Exports.cmake
)

# --- Package Configuration File and Package Version File ---

include (CMakePackageConfigHelpers)

configure_package_config_file (
    ${CMAKE_SOURCE_DIR}/cmake/config-targets.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${FULLY_QUALIFIED_PROJECT_NAME_STRING}Config.cmake
    INSTALL_DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    PATH_VARS FULLY_QUALIFIED_PROJECT_NAME_STRING
)

write_basic_package_version_file (
    ${CMAKE_CURRENT_BINARY_DIR}/${FULLY_QUALIFIED_PROJECT_NAME_STRING}ConfigVersion.cmake
    VERSION       ${CMAKE_PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion
)

install (
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${FULLY_QUALIFIED_PROJECT_NAME_STRING}Config.cmake
        ${CMAKE_CURRENT_BINARY_DIR}/${FULLY_QUALIFIED_PROJECT_NAME_STRING}ConfigVersion.cmake
    DESTINATION ${TNOT_INSTALL_CMAKEDIR}
)
