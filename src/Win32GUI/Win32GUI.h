/*!
    @file
    Helper functions for the Win32-API/GUI.
    
    Copyright © 2006, 2016, 2018, 2021 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::Win32GUI
    @copybrief Win32GUI.h

    @details
    - Provided as a 'header-only library' (thus the inline specifiers).
    - Only tested under Microsoft Windows.
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_E9F729E8_423C_4B20_9ACD_16A00B253BF4
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_E9F729E8_423C_4B20_9ACD_16A00B253BF4
 
#ifdef _WIN32

#include <iomanip>
#include <sstream>
#include <windows.h>

namespace NiftyOddity::Toolkit::Win32GUI
{

// Flags for CenterWindow():
#define MONITOR_CENTER   0x0001 // Center rectangle to monitor.
#define MONITOR_CLIP     0x0000 // Clip rectangle to monitor.
#define MONITOR_WORKAREA 0x0002 // Use monitor's working area.
#define MONITOR_AREA     0x0000 // Use monitor's entire area.

/*!
    Clip or center window to monitor.
    
    Based on sample code from the Platform SDK: Windows GDI: "Positioning Objects on a Multiple Display Setup".
    "The most common problem apps have when running on a multimonitor system is that they 'clip' or
    'pin' windows based on the SM_CXSCREEN and SM_CYSCREEN system metrics.
    Because of app compatibility reasons these system metrics return the size of the primary monitor."
    
    @param hwnd
    Handle to the window.
    
    @param flags
    Can be one of these values: MONITOR_CENTER, MONITOR_CLIP, MONITOR_WORKAREA or MONITOR_AREA (see code).
*/

inline
void centerWindow (HWND hwnd, UINT flags)
{
    RECT wnd_rect;
    GetWindowRect(hwnd, &wnd_rect);
    
    HMONITOR    hMonitor;
	MONITORINFO mi;
	RECT        rc;
	int         width   = wnd_rect.right  - wnd_rect.left;
	int         height = wnd_rect.bottom - wnd_rect.top;
	
	hMonitor = MonitorFromRect(&wnd_rect, MONITOR_DEFAULTTONEAREST); // Get the nearest monitor to the passed rect.
	
	// Get the work area or entire monitor rect.
	mi.cbSize = sizeof(mi);
	GetMonitorInfo(hMonitor, &mi);
	
	if (flags & MONITOR_WORKAREA)
		rc = mi.rcWork;
	else
		rc = mi.rcMonitor;
	
	// Center or clip the passed rect to the monitor rect
	if (flags & MONITOR_CENTER)
	{
		wnd_rect.left   = rc.left + (rc.right  - rc.left - width) / 2;
		wnd_rect.top    = rc.top  + (rc.bottom - rc.top  - height) / 2;
		wnd_rect.right  = wnd_rect.left + width;
		wnd_rect.bottom = wnd_rect.top  + height;
	}
	else
	{
		wnd_rect.left   = max(rc.left, min(rc.right - width,  wnd_rect.left));
		wnd_rect.top    = max(rc.top,  min(rc.bottom - height, wnd_rect.top));
		wnd_rect.right  = wnd_rect.left + width;
		wnd_rect.bottom = wnd_rect.top  + height;
	}
    
    SetWindowPos(hwnd, NULL, wnd_rect.left, wnd_rect.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
}


/*!
    Maps the standard error codes to the corresponding description string.
    
    @note
    Calls `FormatMessageA()` (ASCII) explicitly and therefore only narrow strings are used.
    
    @return
    A text string that contains the error code and the assoicated descriptive text.
*/

inline
std::string getErrorDescription ()
{
	DWORD  error_code = ::GetLastError();
	LPSTR message_buffer;
	
	FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR) &message_buffer, 0, NULL);
	
	std::ostringstream formatted_error_code;
	formatted_error_code << "0x" << std::hex << std::setfill('0') << std::setw(4) << std::uppercase << error_code;
	std::string out (formatted_error_code.str() + ": " + message_buffer);
	
	LocalFree(message_buffer);
	return out;
}

} // namespace

#endif // _WIN32

#endif // include guard