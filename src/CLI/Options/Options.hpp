/*!
    @file
    Free functions to handle options that are given to a program by the command line interface.
    
    Copyright © 2016-2018, 2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::CLI::Options
    @copybrief Options.hpp

    @details
    - Provided as a 'header-only library' (thus the inline specifiers).
    - Only tested under Microsoft Windows.

    # Usage

    Check out `test.cpp` to see how to integrate it in your program.

    ## Supported option styles

    Description                 | Example                              | Function?                    
    :---------------------------|:-------------------------------------|:-----------------------------
    No argument or help switch: | `C:\>example.exe /?`                 | Use _isHelpSwitch()_         
    Simple switch:              | `C:\>example.exe /sw1`               | Use _isSwitch()_             
    Key with assigned value:    | `C:\>example.exe sw3=foo`            | Use _isKeyValue()_           
    Switch with assigned value: | `C:\>example.exe /sw4=foo`           | Use _isSwitchWithValue()_    
    Switch with key & value:    | `C:\>example.exe /sw5 foo="bar baz"` | Use _isSwitchWithKeyValue()_ 
    Switch with value:          | `C:\>example.exe /sw6 "foo bar baz"` | Use _isSwitchAndValue()_     


    ### Argument order and occurrences

    The order of the arguments does not matter:

        C:\>example.exe /sw2 sw3:foo /sw1

        Switch 2
        Key sw3 has value foo
        Switch 1

    Multiple occurrences of the same option are OK:
    All appearances will be noted; it's up to the application to handle such cases.

    ### Case sensitivity

    Case sensitivity can individually be enabled/disabled for each function call.


    ### Prefix character for the options

    You can use either "/" or "-" (or mix both styles) as a prefix character for an option:

        C:\>example.exe /a -b


    ### Assignment character for values

    You can use either "=" or ":" (or mix both styles) as a character where value assignment is expected:
      
        C:\>example.exe /a=foo /b key:value


    ### Space characters

    Spaces are OK, but must be enclosed in quotation marks; and nesting those is not possible.

        C:\>example.exe /a b="a value" "a key"=100 "another key"="another value"


    ### Combination/Folding

    The combination (or folding) of one-character arguments is currently _not_ supported and is
    also not planned (hard to detect, unless you know all paramters in advance): Is it -abc (="abc") or -a + -b + -c?)
      
    Example                    | Remark
    :--------------------------|:----------------------------------------------------
    `C:\>example.exe -a -b -c` | Three  seperate arguments: "a", "b" and "c".
    `C:\>example.exe -abc`     | Represents just one (distinct) argument named "abc".


    ### Long and short form options

    The seperation of short and long form options is currently _not_ supported and
    is also not planned (a "long-form" can be already be implement as a normal parameter: -long-form (TODO is hyphen OK?))

    The following would be two separate options (and the latter would have to be handled explictly):

    _Not_ the same!      ||
    ---------------------|--------------------------------------
    `C:\>example.exe -w` | `C:\>example.exe --word`


    ### Examples

    (Please take also a look in test/demo program.)

    A simple (on) switch:

        /opt1
        -EnableFoo

    A key option with an assigned value:
    Note: There is no prefix ('-' or '/') character at the beginning!

        o2=SomeValue
        mode:Random
        opt3="Some value"

    A switch option with an assigned value:

        /SetFoo=1
        /SetBar:"blah blah"

    A switch option with an associated predefined(!) key and its value:
    (In this example, 'KeyX' and 'KeyY' are hardwired in the code.)

        /o4 KeyX:SomeValue
        /o4 KeyY="Some value"

    A switch with an associated value:
        
        /another_switch "Some value"
        -zone 1234

    @internal
    (2022-02-17)
    - What could be nice: Allowing minimal charachters to id a paramater (like Powershell):
      -id for -identity, if is unique (precondition: All possible parameters must be know in advance, for comparison)
      For that reason: Maybe better a class instead of free functions? (like Python argparse: Setup, etc).

    - Another nice Powershell feature will not be possible: Autocomplete parameters on Tab
      Since our program hasn't started yet at that point in time, so how should it handle
      the TAB key? It's up to the shell/command prompt!
*/

#if !defined (__cplusplus)
    #error "C++ compiler required."
#endif

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_FEFD1096_DA5F_44DB_9A7F_4424A0FD3AC9
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_FEFD1096_DA5F_44DB_9A7F_4424A0FD3AC9
 
#include <string>

namespace NiftyOddity::Toolkit::CLI::Options
{

/*!
    Determines whether the input is the specified switch option (or not).
    
    @param in               The command line argument that will be analyzed, e.g. argv[i].
	@param key              The plain name of the parameter (key), without the assignment characters '=' or ':'.
	@param case_insensitive Control whether the should be interpreted case insensitive or not.
    
    @retval true  if the input was identified as a the specified switch option.
    @retval false if the input wasn't identified as the specified switch option.

    @details
    Usage: a.exe {/|-}switch

    The parameter _key_ can be handled case sensitive or case insensitive, depending on the function argument.
*/

inline
bool isSwitch (const char* in, const char* key, bool case_insensitive = true)
{
	std::string in_str(in);
	std::string key_str(key);

	if (case_insensitive)
	{
		for (std::string::iterator i = in_str.begin()+1; i != in_str.end(); ++i)
			*i = ::toupper(*i);
		
		for (std::string::iterator j = key_str.begin(); j != key_str.end(); ++j)
			*j = ::toupper(*j);
	}

	std::string key_v1 ("/" + key_str);
	std::string key_v2 ("-" + key_str);
	
	if ((in_str.compare(key_v1) == 0) || (in_str.compare(key_v2) == 0))
		return true;
	else
		return false;
}


/*!
    Determines whether the input is the specified switch option, and if so, returns its value in an out-parameter.
    
    @param[in]  in               The command line argument that will be analyzed, e.g. argv[i].
	@param[in]  key              The plain name of the parameter (key), without the assignment characters '=' or ':'.
	@param[out] out              Will contain the value associated with the key.
    @param[in]  case_insensitive Control whether the should be interpreted case insensitive or not.
    
    @retval true  if _key_ is found in the input string (_in_); _out_ will contain the value of the key.
    @retval false if the key wasn't found.

    @details
    a.exe {/|-}switch{=|:}{value|"long value with spaces"}

    'Key' can be handled case sensitive or case insensitive, depending on the function argument.
*/

inline
bool isSwitchWithValue (const char* in, const char* key, std::string* out, bool case_insensitive = true)
{
	std::string in_str(in);
	std::string key_str(key);

	if (case_insensitive)
	{
		for (std::string::iterator i = in_str.begin()+1; i != in_str.end(); ++i)
		{
			if (*i == '=' || *i == ':')
				break;
			else
				*i = ::toupper(*i);
		}
		
		for (std::string::iterator j = key_str.begin(); j != key_str.end(); ++j)
		{
			*j = ::toupper(*j);
		}
	}

	std::string key_v1 ("/" + key_str + "=");
	std::string key_v2 ("/" + key_str + ":");
	std::string key_v3 ("-" + key_str + "=");
	std::string key_v4 ("-" + key_str + ":");

	size_t found_v1 = in_str.find(key_v1);
	size_t found_v2 = in_str.find(key_v2);
	size_t found_v3 = in_str.find(key_v3);
	size_t found_v4 = in_str.find(key_v4);

	if (found_v1 != std::string::npos || found_v2 != std::string::npos || found_v3 != std::string::npos || found_v4 != std::string::npos)
	{
		size_t key_length = key_v1.length();
		*out = in_str.substr(key_length);
		return true;
	}
	else
	{
		return false;
	}
}


/*!
    Determines whether the input is the specified key, and if so, returns its value in an out-parameter.

	@param[in]  in               The command line argument that will be analyzed, e.g. argv[i].
	@param[in]  key              The plain name of the parameter (key), without the assignment characters '=' or ':'.
	@param[out] out              Will contain the value associated with the key.
    @param[in]  case_insensitive Control whether the should be interpreted case insensitive or not.
    
    @retval true  if _key_ is found in the input string (_in_); _out_ will contain the value of the key.
    @retval false if the key wasn't found.

    @details
    Usage: a.exe key{=|:}{value|"long value with spaces"}
    
    The parameter _key_ can be handled case sensitive or case insensitive, depending on the function argument.
    
    The parameter _key_ must begin with an alphabetic or numeric character, meaning "+ABC=123" or "*ABC=123" will _not_ be valid.
    
    Tip: To handle "/ABC=123", use isSwitchWithValue().
*/

inline
bool isKeyValue (const char* in, const char* key, std::string* out, bool case_insensitive = true)
{
	if (::isalnum(in[0]))
	{
		std::string in_str(in);
		std::string key_str(key);
		std::string str_value(in);
	
		if (case_insensitive)
		{
			for (std::string::iterator i = in_str.begin(); i != in_str.end(); ++i)
			{
				if (*i == '=' || *i == ':')
					break;
				else
					*i = ::toupper(*i);
			}
		
			for (std::string::iterator j = key_str.begin(); j != key_str.end(); ++j)
			{
				*j = ::toupper(*j);
			}
		}
	
		std::string key_v1 (key_str + "=");
		std::string key_v2 (key_str + ":");

		size_t found_v1 = in_str.find(key_v1);
		size_t found_v2 = in_str.find(key_v2);

		size_t key_length = key_v1.length(); // One value is enough: v1 and v2 have the same format and thus length.
		
		if ((in_str.compare(0, key_length, key_v1) == 0) || (in_str.compare(0, key_length, key_v2) == 0))
		{
			*out = in_str.substr(key_length);
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}


/*!
    Determines whether the input is the specified switch option with a key/value pair, and if so, returns its value in an out-parameter.
    
    @param[in]  cmdline
	@param[in]  i
	@param[in]  parent_switch    The switch option that must match; else the key/value doesn't belong to this option.
    @param[in]  key              The plain name of the parameter (key), without the assignment characters '=' or ':'.
	@param[out] out              Will contain the value associated with the key.
    @param[in]  case_insensitive Control whether the should be interpreted case insensitive or not.
    
    @retval true  if _key_ is found in the input string (_in_); _out_ will contain the value of the key.
    @retval false if the key wasn't found.

    @details
    a.exe {/|-}switch key{=|:}{value|"long value with spaces"}
*/

inline
bool isSwitchWithKeyValue (char* cmdline [], int i, const char* parent_switch, const char* key, std::string* out, bool case_insensitive = true)
{
	if (isSwitch(cmdline[i], parent_switch, case_insensitive))
	{
		for (int j = i+1; cmdline[j] != NULL; j++)
		{
			if (isKeyValue(cmdline[j], key, out, case_insensitive))
			{
				return true;
			}
		}
	}

	return false;
}


/*!
    Determines whether the input is the specified switch option with a value, and if so, returns its value in an out-parameter.
    
    @param[in]  cmdline
	@param[in]  i
	@param[in]  parent_switch    The switch option that must match; else the key/value doesn't belong to this option.
    @param[in]  key              The plain name of the parameter (key), without the assignment characters '=' or ':'.
	@param[out] out              Will contain the value associated with the key.
    @param[in]  case_insensitive Control whether the should be interpreted case insensitive or not.
    
    @retval true  if _key_ is found in the input string (_in_); _out_ will contain the value of the key.
    @retval false if the key wasn't found.

    @details
    a.exe {/|-}switch {value|"long value with spaces"}
*/

inline
bool isSwitchAndValue (char* cmdline [], int i, const char* parent_switch, std::string* out, bool case_insensitive = true)
{
	if (isSwitch(cmdline[i], parent_switch, case_insensitive))
	{
		if (cmdline[i+1] != NULL && (cmdline[i+1][0] != '/' && cmdline[i+1][0] != '-'))
		{
			std::string retval(cmdline[i+1]);
			*out = retval;
			return true;
		}
	}
	
	return false;
}


/*!
    Determines whether the input matches a predefined "help" identifier.
    
    @param argc
	@param in
    @param case_insensitive Control whether the should be interpreted case insensitive or not.

    @retval true  if the argument for _in_ is detected as an identifier for "help"; else returns false.
    @retval false if it wasn't detected as a switch character for "help".
    
    @details
    a.exe {/|-}switch {value|"long value with spaces"}
*/

inline
bool isHelpSwitch (int argc, char* in, bool case_insensitive = true)
{
	if (argc < 2 || isSwitch(in, "h", case_insensitive) || isSwitch(in, "help", case_insensitive) || isSwitch(in, "?", case_insensitive))
		return true;
	else
		return false;
}

} // namespace

#endif // include guard