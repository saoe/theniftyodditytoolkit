/*!
    @file
    Utility/Helper functions for the Command Line Interface (on Windows)
    
    Copyright © 2021-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::CLI
    Namespace for all things related to Command Line Interface stuff (on Windows).
    
    @namespace NiftyOddity::Toolkit::CLI::Utilities
    @copybrief Utilities.hpp
*/

#if !defined (__cplusplus)
    #error "C++ compiler required."
#endif

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_9EE0BD68_52FB_41ED_8176_FEC42AE3CCEF
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_9EE0BD68_52FB_41ED_8176_FEC42AE3CCEF
 
#include <string>
#include <iomanip>
#include <chrono>
#include <thread>

namespace NiftyOddity::Toolkit::CLI::Utilities
{

/*!
    Show a spinning busy cursor on the prompt (like: - -> \ -> | -> /).
    
    Spins the cursor only one time per function call; repeat invocation in a loop in your application.
*/

static inline
void showBusyCursor (void)
{
    std::chrono::milliseconds ms(100);
    std::cout << '-' << '\b';
    std::this_thread::sleep_for(ms);
    std::cout << '\\' << '\b';
    std::this_thread::sleep_for(ms);
    std::cout << '|' << '\b';
    std::this_thread::sleep_for(ms);
    std::cout << '/' << '\b';
    std::this_thread::sleep_for(ms);
    std::cout << ' ' << '\b';
    std::this_thread::sleep_for(ms);
}


/*!
    Show a progress bar at a specified position (percentage value).
    
    Valid range is 0-100 (%); per call a progress bar is shown at the specific percent.
    Use a loop with increasing values in your application code to show the actual progress.
    
    @param percent
    The percentage value which the progress bar should display.
*/

static inline
void showProgressBar (int percent)
{
    if (percent > 100) { percent = 100; }
    std::cout << '\r' << std::setw(3) << percent << "% ["<< std::string(percent, 'o') << std::string(100 - percent, ' ') << "]";
    std::this_thread::sleep_for(std::chrono::milliseconds(100));    
}

} // namespace

#endif // include guard