/*!
    @file
    Implementation of "Console", a utility class for the Command Line Interface (Windows-specific)

    Copyright © 2021-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::CLI::Console
    @copybrief Console.hpp
*/

#if !defined (__cplusplus)
    #error "C++ compiler required."
#endif

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_40354B33_1068_48AA_A811_838841AA8D51
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_40354B33_1068_48AA_A811_838841AA8D51

#ifdef _WIN32

#include <string>
#include <iomanip>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <windows.h>

//! Docu?
#ifdef ENABLE_ANSI_ESCAPE_CODES
    static bool Use_ANSI_Escape_Codes = true;
#else
    static bool Use_ANSI_Escape_Codes = false;
#endif

namespace NiftyOddity::Toolkit::CLI::Console
{
/* TODO -- Put in class below!

static inline
void saveCursorPosition (void)
{
    printf("\033%d", 7);
}


static inline
void restoreCursorPosition (void)
{
    printf("\033%d", 8);
}


static inline
void getCursorPosition (int* row, int* col)
{
    printf("\033[6n");
    char buff[128];
    int indx = 0;
    
    for(;;)
    {
        int cc = getchar();
        buff[indx] = (char)cc;
        indx++;
        if(cc == 'R') {
            buff[indx + 1] = '\0';
            break;
        }
    }    
    sscanf(buff, "\033[%d;%dR", row, col);
    fseek(stdin, 0, SEEK_END);
}

*/

// -------------------------------------------------------------------------------------------------

/*!
    Utility class for the Command Line Interface (on Windows)
    
    @details
    Provides support for the most common ANSI escape code sequences on the (stanard) output stream.
    
    Uses ANSI escape codes (if the compile definition ENABLE_ANSI_ESCAPE_CODES is set!) for color and other gimmicks.
    
    Before any function from this group will show any effect, the Windows console must be prepared;
    this happens automatically by the constructor.
    
    After calling a function from this group, the next printf() or std::cout statement(s) that follows
    will use the changed state (e.g. a changed background color).
    
    Each display attribute remains in effect until a following occurrence of SGR resets it; i.e. the
    state will remain until it is again changed/reset/disabled/destroyed (by the destructor).
    
    The Microsoft Windows Console Host (used by the Command Prompt) supports ANSI escape codes since
    Windows 10 version 1511 (from the year 2016).
    
    Note that different consoles/terminals support different features (or not), and those maybe differently:
    Even on Windows, there is a difference on how the standard cmd.exe host and the Powershell interprete
    simple things like a yellow background.
    
    @todo
    The ctor is just needed for initialization, the functions are static -- is that a good design or required?
    
    @see
    - https://en.wikipedia.org/wiki/ANSI_escape_code
    - https://notes.burke.libbey.me/ansi-escape-codes/
    - https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
    - https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
    - https://solarianprogrammer.com/2019/04/08/c-programming-ansi-escape-codes-windows-macos-linux-terminals/
    - https://github.com/sol-prog/ansi-escape-codes-windows-posix-terminals-c-programming-examples
    - https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
    - https://docs.microsoft.com/de-de/windows/console/console-virtual-terminal-sequences
*/
class Console
{
	public:
        /*!
            The constructor prepares the console/terminal for ANSI escape codes.
    
            The Windows Console Host (which is used by the Command Prompt) supports ANSI escape codes
            since Windows 10 version 1511 (from the year 2016).
            
            @todo
            The ctor is just needed for initialization, the functions are static -- is that a good design or required?
        */
		Console (bool use_ansi_escape_codes = Use_ANSI_Escape_Codes)
		{
            Use_ANSI_Escape_Codes = use_ansi_escape_codes;
            
            // Saving the current values to restore them later again.
            if (Use_ANSI_Escape_Codes)
            {
                DWORD outmode = 0;
                standard_output = GetStdHandle(STD_OUTPUT_HANDLE);
                GetConsoleScreenBufferInfo(standard_output, &CSBInfo);
                if (standard_output == INVALID_HANDLE_VALUE) { exit(GetLastError()); }
                if (!GetConsoleMode(standard_output, &outmode)) { exit(GetLastError()); }
                outmode_backup = outmode;
                outmode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING; // Enable ANSI escape codes
                if (!SetConsoleMode(standard_output, outmode)) { exit(GetLastError()); }
            }
		}

        /*!
            Restore the console/terminal to its previously saved state (if ANSI escape codes were used).
        */
		~Console ()
		{
			if (Use_ANSI_Escape_Codes)
            {
                
                SetConsoleTextAttribute(standard_output, CSBInfo.wAttributes);
            
                printf("\033[0m");
                if (!SetConsoleMode(standard_output, outmode_backup)) { exit(GetLastError()); }
            }
		}

        enum struct TextColor : int           // ANSI color codes
        {
            ResetAll = 0, // Reset background and foreground colors.
            Black = 30,
            Red,
            Green,
            Yellow ,
            Blue,
            Magenta,
            Cyan,
            White,
            Reset = 39, // Reset just the background to default.
            BrightBlack = 90,
            BrightRed,
            BrightGreen,
            BrightYellow,
            BrightBlue,
            BrightMagenta,
            BrightCyan,
            BrightWhite
        };

        enum struct BackgroundColor : int     // ANSI color codes
        {
            ResetAll = 0, // Reset background and foreground colors.
            Black = 40,
            Red,
            Green,
            Yellow,
            Blue,
            Magenta,
            Cyan,
            White,
            Reset = 49, // Reset just the background to default.
            BrightBlack = 100,
            BrightRed,
            BrightGreen,
            BrightYellow,
            BrightBlue,
            BrightMagenta,
            BrightCyan,
            BrightWhite
        };

        enum struct Clear
        {
            Screen,
            FromCursorToEndOfScreen,
            FromCursorToStartOfScreen,
            EntireScreen,
            Line,
            FromCursorToEndOfLine,
            FromCursorToStartOfLine,
            EntireLine
         };
        
        //! `Italic`, `Blink` and `Strikethrough` are not supported under Windows 10 (yet).
        enum struct Style                     // ANSI codes
        {
            Reset = 0, // Reset all colors and styles.
            Bold = 1, // ... or rather "bright", on most terminals.
            Faint = 2,
            Italic = 3,
            Underline = 4,
            Blink = 5,
            Invert = 7,
            Strikethrough = 9
        };

        enum struct Status : int
        {
            // Background color stays as it is.
            Reset   = 0,
            Error   = TextColor::BrightRed,
            OK      = TextColor::BrightGreen,
            Warning = TextColor::BrightYellow,
            Info    = TextColor::BrightCyan
        };
        
        enum struct CursorNavigation
        {
            Home, //!< Move to home postion: line 0, column 0.
            Up,
            Down,
            Left,
            Right,
            LineUp,
            LineDown,
            GoToColumn,
        };   
        
		// Making the overloaded operator to friends of this class, so that they can access the private members.
		friend std::ostream& operator<< (std::ostream& s, const Status& status);
        friend std::ostream& operator<< (std::ostream& s, const BackgroundColor& color);
        friend std::ostream& operator<< (std::ostream& s, const TextColor& color);
        friend std::ostream& operator<< (std::ostream& s, const Style& style);
        friend std::ostream& operator<< (std::ostream& s, const Clear& mode);
        friend std::ostream& operator<< (std::ostream& s, const CursorNavigation& direction);

        /*!
            Move the cursor in the specified direction, by 1 or more units.
            
            @remark
                To see the desired effect, enough free space must be available. That means
                the cursor should be at a fitting postion, or you should make the space available before
                calling this function. No error will appear, but the cursor will simply stay at the same spot.
                
                @par
                For example, if the cursor is already on the last row and on the first column, a call to go
                one cell down and one cell left will not work, since there is no such space beneath or to
                the left of the cursor. 
        
            @param direction  The direction in which the cursor should move.
            @param units      The amount of units that the cursor should move; the default value is `1`.
            @param AsFunction If set to `true` (default), the function will also output the return value to the std::cout stream.
            
            @return If applicable, the ANSI escape code sequence for this movement, encoded as a std::string.
        */
        static inline
        std::string moveCursor (CursorNavigation direction, int units = 1, bool AsFunction = true)
        {
            return moveCursor_internal(direction, units, AsFunction);
        }
        
        static inline
        std::string moveCursorToPosition (int row, int col, bool AsFunction = true)
        {
            return moveCursorToPosition_internal(row, col, AsFunction);
        }
        
        static inline
        void clear (Clear mode)
        {
            if (Use_ANSI_Escape_Codes)
            {
                std::cout << mode;
            }
        }

	private:
		
        /* The "Select Graphic Rendition (SGR)" subset of codes are triggered by "\ESC[XXXm" commands,
           where the ESC can be written in octal (\033) or in hexadecimal (\x1b); e.g. \033[XXXm or \x1b[XXXm.
           And 'XXX' is a series of semicolon-separated parameters (terminated by the 'm'). */
        static inline const std::string ESC     = "\033";    // ESC as octal (\033); could also be provided in hexadecimal (\x1b).
        static inline const std::string ESC_CSI = ESC + "["; // CSI (Control Sequence Introducer).
        
        static HANDLE                     standard_output;
		static DWORD                      outmode_backup;
        static CONSOLE_SCREEN_BUFFER_INFO CSBInfo;
        
        static inline
        std::string moveCursor_internal (CursorNavigation direction, int units, bool AsFunction)
        {
            std::string str;
            
            if (Use_ANSI_Escape_Codes)
            {
                switch (direction)
                {
                    case CursorNavigation::Home:
                        str = ESC_CSI + "H";
                        break;
                    case CursorNavigation::Up:
                        str = ESC_CSI + std::to_string(units) + "A";
                        break;
                    case CursorNavigation::Down:
                        str = ESC_CSI + std::to_string(units) + "B";
                        break;
                    case CursorNavigation::Right:
                        str = ESC_CSI + std::to_string(units) + "C";
                        break;
                    case CursorNavigation::Left:
                        str = ESC_CSI + std::to_string(units) + "D";
                        break;
                    case CursorNavigation::LineDown:
                        str = ESC_CSI + std::to_string(units) + "E";
                        break;
                    case CursorNavigation::LineUp:
                        str = ESC_CSI + std::to_string(units) + "F";
                        break;
                    case CursorNavigation::GoToColumn:
                        str = ESC_CSI + std::to_string(units) + "G";
                        break;
                }
                
                if (AsFunction)
                    std::cout << str;
            }
            return str;
        }

        static inline
        std::string moveCursorToPosition_internal (int line, int column, bool AsFunction)
        {
            std::string str;
            if (Use_ANSI_Escape_Codes)
            {
                str = ESC_CSI + std::to_string(line) + ";" + std::to_string(column) + "F";      
                if (AsFunction)
                {
                    std::cout << str;
                }
            }
            return str;
        }
};

// Non-const static member variables must be defined (and initalized) outside of the class definition,
// because there can only be one instance in the program.
HANDLE                     Console::standard_output = 0;
DWORD                      Console::outmode_backup;
CONSOLE_SCREEN_BUFFER_INFO Console::CSBInfo;

//! Overloaded operator to set the following text according to the predefined 'status' styles.
static inline
std::ostream& operator<< (std::ostream& s, const Console::Status& status)
{
    if (Use_ANSI_Escape_Codes)
    {
        s << Console::ESC_CSI << static_cast<int>(status) << "m";
    }
	return s;
}

//! Overloaded operator to set the background color for the following text.
static inline
std::ostream& operator<< (std::ostream& s, const Console::BackgroundColor& color)
{
    if (Use_ANSI_Escape_Codes)
    {
        s << Console::ESC_CSI << static_cast<int>(color) << "m";
    }
	return s;
}

//! Overloaded operator to set the text color for the following text.
static inline
std::ostream& operator<< (std::ostream& s, const Console::TextColor& color)
{
    if (Use_ANSI_Escape_Codes)
    {
        s << Console::ESC_CSI << static_cast<int>(color) << "m";
    }
    return s;
}

//! Overloaded operator to set the style for the following text.
static inline
std::ostream& operator<< (std::ostream& s, const Console::Style& style)
{
    if (Use_ANSI_Escape_Codes)
    {
        s << Console::ESC_CSI << static_cast<int>(style) << "m";
    }
    return s;
}

//! Overloaded operator to move the cursor (by one cell).
static inline
std::ostream& operator<< (std::ostream& s, const Console::CursorNavigation& direction)
{
    if (Use_ANSI_Escape_Codes)
    {
       s << Console::moveCursor(direction, 1, false);
        // Since we cannot provide additional parameters to a binary operator, each invocation of this
        // will move the cursor just one cell in the given direction.
    }
    return s;
}

//! Overloaded operator to clear the screen accordingt to the mode.
static inline
std::ostream& operator<< (std::ostream& s, const Console::Clear& mode)
{
    if (Use_ANSI_Escape_Codes)
    {
        switch (mode)
        {
            case Console::Clear::Screen:
                s << Console::ESC_CSI << "J";
                break;
            case Console::Clear::FromCursorToEndOfScreen:
                s << Console::ESC_CSI << "0J";
                break;
            case Console::Clear::FromCursorToStartOfScreen:
                s << Console::ESC_CSI << "1J";
                break;
            case Console::Clear::EntireScreen:
                s << Console::ESC_CSI << "2J";
                break;
            case Console::Clear::Line:
                s << Console::ESC_CSI << "K";
                break;
            case Console::Clear::FromCursorToEndOfLine:
                s << Console::ESC_CSI << "0K";
                break;
            case Console::Clear::FromCursorToStartOfLine:
                s << Console::ESC_CSI << "1K";
                break;
            case Console::Clear::EntireLine:
                s << Console::ESC_CSI << "2K";
                break;
        }
    }
    return s;
}

} // namespace

#endif // _WIN32

#endif // include guard