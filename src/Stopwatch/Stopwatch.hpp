/*!
    @file
    Implementation of class 'Stopwatch'.
    
    Copyright © 2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)

    @namespace NiftyOddity::Toolkit::Stopwatch
    @copybrief Stopwatch.hpp
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_4744945E_DD3E_47E5_B310_B0CDCA50FBBD
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_4744945E_DD3E_47E5_B310_B0CDCA50FBBD

#include <algorithm>
#include <chrono>
#include <iostream>
#include <string>
#include <vector>

namespace NiftyOddity::Toolkit
{

class Stopwatch
{
    public:
        using clock = std::chrono::steady_clock;

        // How the ticks of the clock should be stored as a duration (time-span):
        using MicrosecondDuration = std::chrono::duration<long double, std::ratio<1,1000000>>;
        using MillisecondDuration = std::chrono::duration<long double, std::ratio<1,1000>>;
        using SecondDuration      = std::chrono::duration<long double>;
        using MinuteDuration      = std::chrono::duration<double, std::ratio<60>>;
        using HourDuration        = std::chrono::duration<double, std::ratio<60*60>>;

        //! The result of the stopwatch run: elapsed time, splits and misc. average values.
        struct Result
        {
            std::vector<double> laps_in_ms;
            int lap_count;

            long double microseconds;
            long double milliseconds;
            long double seconds;
            double minutes;
            double hours;

            double mean_in_ms;
            double median_in_ms;
            double midrange_in_ms;
        };

        Stopwatch ()
        {
            /* All the initialization steps happen in the private reset() member function,
            since such a function is needed for later anyway, let's not repeat in the same
            actions in the function and the constructor (... member initializer list);
            might go out of sync. */
            reset();
        }

        void start ()
        {
            if (status == Status::Uninitialized)
            {
                // First time reset() already happend in the constructor.
                start_time_point = start_split_time_point = clock::now();
                status = Status::Running;
            }
            else if (status == Status::Stopped)
            {
                reset();
                start_time_point = start_split_time_point = clock::now();
                status = Status::Running;
            }
        }
        
        void pause ()
        {
            if (status == Status::Running)
            {
                auto now = clock::now();
                begin_pause = now;
                active_time += std::chrono::duration_cast<MillisecondDuration>(now - start_time_point);
                status = Status::Paused;
            }
        }

        void resume ()
        {
            if (status == Status::Paused)
            {
                auto now = clock::now();
                start_time_point = now;
                total_inactive_time += std::chrono::duration_cast<MillisecondDuration>(now - begin_pause);
                latest_inactive_time = std::chrono::duration_cast<MillisecondDuration>(now - begin_pause);
                status = Status::Running;
            }
        }

        //! Save the duration of a lap as an intermediate result.
        void split ()
        {
            // When the split time button is pressed while the watch is running, it allows the elapsed time to
            // that point to be read, but the watch continues running to record the total elapsed time.
            if (status == Status::Running)
            {
                auto now = clock::now();
                laps_in_ms.push_back(std::chrono::duration_cast<MillisecondDuration>((now - start_split_time_point) - total_inactive_time).count());
                start_split_time_point = now; // The old ending is the new beginning...
            }
        }

        void stop ()
        {
            if (status == Status::Running)
            {
                end_time_point = clock::now();
                active_time += std::chrono::duration_cast<MillisecondDuration>(end_time_point - start_time_point);
                
                if (laps_in_ms.empty())
                    laps_in_ms.push_back(std::chrono::duration_cast<MillisecondDuration>((end_time_point - start_split_time_point) - total_inactive_time).count());
                else
                    laps_in_ms.push_back(std::chrono::duration_cast<MillisecondDuration>((end_time_point - start_split_time_point) - latest_inactive_time).count());

                status = Status::Stopped;
            }
        }

        Result result ()
        {
            if (!(status == Status::Stopped))
                stop();

            Result r{};

            if (status == Status::Stopped)
            {
                r.laps_in_ms = laps_in_ms;
                r.lap_count = laps_in_ms.size();

                r.hours        = std::chrono::duration_cast<HourDuration>(active_time).count();
                r.minutes      = std::chrono::duration_cast<MinuteDuration>(active_time).count();;
                r.seconds      = std::chrono::duration_cast<SecondDuration>(active_time).count();
                r.milliseconds = std::chrono::duration_cast<MillisecondDuration>(active_time).count();
                r.microseconds = std::chrono::duration_cast<MicrosecondDuration>(active_time).count();

                // Calculate misc. 'average' values:
                //
                // 1. The mean (aka the arithmetic mean) is the sum of all the numbers in the set divided by the amount of numbers in the set.
                //    Example: (1+2+2+3+4+7+9) / 7 -> Result: 4.
                double sum = {};
                for (auto i : laps_in_ms) { sum += i; }
                r.mean_in_ms = sum / laps_in_ms.size();
                
                // 2. The median is the middle point of a number set, in which half the numbers are above the median and half are below. 
                //    Example: 1, 2, 2, 3, 4, 7, 9 -> Result: 3.
                std::sort(laps_in_ms.begin(), laps_in_ms.end());
                if (laps_in_ms.size() % 2 == 0)
                {
                    // When there's an even amount of values in the data set.
                    // Add the two middle numbers and divide by 2.
                    // Example: 1, 2, 2, 3, 4, 7 -> (2 + 3)/2 -> Result: 2.5
                    r.median_in_ms = ( (laps_in_ms.at(laps_in_ms.size()/2))
                                 + (laps_in_ms.at(laps_in_ms.size()/2-1))
                               ) / 2;
                }
                else
                {
                    // When there's an odd amount of values in the data set.
                    r.median_in_ms = laps_in_ms.at(laps_in_ms.size() / 2);
                }
                
                // 3. The midrange is the arithmetic mean of the highest and lowest value of a set.
                //    Example: (1+9) / 2 -> Result: 5
                double min = *std::min_element(laps_in_ms.begin(), laps_in_ms.end());
                double max = *std::max_element(laps_in_ms.begin(), laps_in_ms.end());
                r.midrange_in_ms = (min + max) / 2;
            }

            return r;
        }

    private:
        clock::time_point start_time_point,
                          start_split_time_point,
                          end_time_point,
                          begin_pause;

        enum class Status
        {
            Uninitialized,
            Stopped,
            Running,
            Paused
        };

        Status status = Status::Uninitialized;

        std::vector<double> laps_in_ms;

        MillisecondDuration active_time;
        MillisecondDuration total_inactive_time;
        MillisecondDuration latest_inactive_time;

        // Initialize, respectively reset, all the important stuff before starting the watch for the first time
        // (or for a new run, i.e. if one starts/stops the same stopwatch object instance multiple times).
        void reset ()
        {
            // Must be initialized, else it will start with garbage value, which messes up the result).
            start_time_point = {};
            end_time_point = {};
            begin_pause = {};
            active_time = {};
            total_inactive_time = {};
            latest_inactive_time = {};

            status = Status::Stopped;
            laps_in_ms.clear();
        }
};
    
} // namespace
 
#endif // include guard