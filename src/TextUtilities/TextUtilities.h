/*!
    @file
    Helper functions for processing and modifying text (strings).
    
    Copyright © 2021-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::TextUtilities
    @copybrief TextUtilities.h
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_F68B719F_EA4A_465A_B2D3_88CE526D1A8D
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_F68B719F_EA4A_465A_B2D3_88CE526D1A8D
 
#include <iostream>
#include <string>
#include <regex>
#include <vector>

namespace NiftyOddity::Toolkit::TextUtilities
{

/*!
    Substitute a placeholder text in a string with other text value.

    Useful for a given text string _str_, where multiple different placeholders (e.g. _$VAR1$_,
    _{VAR2}_, etc.) need to be replaced with an actual value for further processing.

    This function template helps in that case, because it...
    - can deal with an unspecified amount of arguments (and multiple different placeholders) in the
      input text, since it is implemented as a variadic function template.
    - has an intuitive way (I think) for representing the placeholder/substitution combination by
      using a _std::pair<>_ type for that.

    The main reason for it being a function template is the good support for the arbitary amount of arguments,
    and not so much the aspect of being generic in terms of supported types.

    @note
    Although this is an function template and thus should maybe handle different types, it's currently
    only tested against `std::pair<std::string,std::string>` for `P`.

    @tparam P The type of the pair of _placeholder-&-substitute_ (see note above!)
    
    @param input_string The text string in which the following placeholder(s) should be found.
    @param p            The list of placeholder-&-substitute text pairs;\n
                        the first unit contains the placeholder, the second unit contains the replacement text;\n
                        e.g. `std::pair<std::string,std::string> x("$VAR1$", "The new text");`\n
    @param rest         (_Ignore; it's an implementation detail of this being a variadic function template_)
    
    @return A new string, where the text that matched the regular expression was subsituted with the new text of the pair.
    
    __Usage example__
    
    @code
    std::string str = "Variable 1 is $var1$\nVariable 2 is $var2$\nVariable 3 is $var3$";

    std::pair<std::string,std::string> a("$var1$", "AAA");
    std::pair<std::string,std::string> b("$var2$", "BBB");
    std::pair<std::string,std::string> c("$var3$", "CCC");

    std::cout << NiftyOddity::Toolkit::TextUtilities::replacePlaceholder(str, a, b, c);
    
    // -- Output: --
    // Variable 1 is AAA
    // Variable 2 is BBB
    // Variable 3 is CCC
    @endcode

    @see
    Articles that were very helpful for my understanding of how variadic-templates work:
    - https://kevinushey.github.io/blog/2016/01/27/introduction-to-c++-variadic-templates/
    - https://bolt-software.com/2014-04-13-cpp-variadic-templates/

    @internal
    This is a variadic function template (i.e. it can deal with an unspecified amount of arguments,
    by a kind of recursion [1]), and uses a regular expression to find the text.
    
    [1] It has a recursive appearence and quality, but the compiler is in fact treating each variation
        as its own method (i.e. template instantiation), since templates are resolved at compile-time.
    
    Other obvious alternatives (e.g. normal functions) seemed to unelegant/error-prone in comparison:
    - What if the amount of arguments don't fit?  
      `func(input, placeholder1, value1, placeholder2); // Bug: Missing a matching value2.`
    - What if the correct order is messed up?  
      `func(input, placeholder1, placeholder2, value1, value2);`  
      `func(input, placeholder1, value1, placeholder2, value2);`
    @endinternal
*/
template <typename P, typename... Rest>
std::string replacePlaceholder (std::string input_string, P p, Rest... rest)
{
    // Some characters need to be escaped before they can be used to build a regular expression.
    // Also note that this constant raw string literal here is a regex itself:
    // The characters are enclosed in square brackets (RegEx: "character class"), and therein, the square brackets need to be escaped too!
    static std::regex const escape_chars { R"([$(){}\[\]])" };
    
    // Prepare the placeholder for the regular expression (using raw string literals):
    // 1. Escape some characters that are allowed as placeholder marker, so that they can be used in the following RegEx.
    // 2. Then sanitize the placeholder with an escape-backslash (= \) for the dollar-sign, plus the matched substring (= $&). */
    std::string sanitized_string = std::regex_replace(p.first, escape_chars, R"(\$&)" );

    if constexpr (sizeof...(rest) == 0)
    {
        // This is used when there's just a single item/argument left at the end.
        return std::regex_replace(input_string, std::regex(sanitized_string), p.second);
    }
    else
    {
        // Expand the template's 'Parameter Pack' (i.e. the arguments of the 'Rest' template parameter)
        // by using the splat syntax (i.e. "..."), until there is only one argument remaining.
        // That single argument will then be handled by the matching special function template below.
        return replacePlaceholder(std::regex_replace(input_string, std::regex(sanitized_string), p.second), rest...);
    }
}


// -------------------------------------------------------------------------------------------------

/*!
    Split a given string into chunks.

    A string can be split when each component is separated by a delimter character (e.g. semicolon, comma, space).
    A component itself can contain space, if it is enclosed in double-quotation-marks.
    
    For example, this one will produce four chunks:
    
    @code
    std::vector<std::string> out = splitString("value-1;string with space;even more values", ';');
    @endcode
    
    @param delimiter    The character that denotes the delimiter for the chunks;
                        by default (if no argument is given), the space character is used as the delimiter.
    @param input_string Represents the string that should be split.
    
    @return A vector of strings, each element of that vector represents a chunk of the splitted input value.
    
    @internal
    2021-07-19 (so): Created here, based on the old splitValue() from InitFile; with some changes.
    2021-11-10 (so): API has changed (order of parameters); can now better handle strings with spaces, without much escaping.
*/

inline
std::vector<std::string> splitString (std::string input_string, const char delimiter = ' ')
{
    std::string::iterator end = input_string.end();
    
    std::vector<std::string> output;
    std::string temp;
    
    for (std::string::iterator pos = input_string.begin(); pos != end; ++pos)
    {
        if (isspace(*pos) || *pos == delimiter)
        {
            continue;
        }
        else
        {
            if (*pos == '"' && (pos+1) != end)
            {
                ++pos;
                
                while (*pos != '"' && (pos+1) != end)
                {
                    temp += *pos;
                    ++pos;
                }
                
                output.push_back(temp);
                temp.clear();
            }       
            else
            {
                while (*pos != delimiter && (pos+1) != end)
                {
                    temp += *pos;
                    ++pos;
                }
            
                // The previous loop exits one character before the end, thus the correction.
                if (*pos != delimiter)
                    temp += *pos;
                
                output.push_back(temp);
                temp.clear();
            }
        }
    }

    return output;
}


/*!
    Join a vector of strings into one string, each chunk separated by a delimiter character.
    
    @param delimiter The character that denotes the delimiter for the chunks; the default character is the semicolon.
    @param input     The vector of strings that should be combined.
    
    @return A string that is a combination of the input strings, each entry separated by the delimiter character.
    
    @internal
    2021-07-19 (so): Created here, based on the old joinValue() from InitFile; with some changes.
*/

inline
std::string joinStrings (const char delimiter, std::vector<std::string> input)
{
    std::string s; // As far as I know: Standard guarantees the string to be initialized with 'zero'/non-garbage value.
    
    if (input.empty())
    {
        return s;
    }
    else
    {
        for (std::vector<std::string>::iterator pos = input.begin(); pos != input.end(); ++pos)
        {
            if (pos == (input.end()-1) || input.size() == 1)
                s.append(*pos);
            else
                s.append(*pos + std::string(1, delimiter));
        }   
        return s;
    }
}


/*!
    Join multiple strings into one string, each chunk separated by a delimiter character.
    
    Usage example:
    
    @code
    std::string x = joinStrings(';', "abc", "def", "efg");
    std::cout << x << std::endl;
    @endcode
    
    @tparam Argument  Is the currently expanded argument item; can be ignored by the user.
    @tparam Arguments Is the parameter pack of the remaining arguments, which that will be expanded; can be ignored by the user.
    
    @param delimiter Defines the character which will separate the combined strings within the single output string;
                     e.g. ';', ',', ' ' or any other single character.
    @param current   The string(s) that should be combined.
    @param remaining The arguments that still remain; can be ignored by the user (will be handled by the function).
    
    @return A single string, which is a combination of all provided strings, separated by the delimiter character.
    
    @internal
    Implemented as a variadic template function (parameter pack), so that it
    can accept an unknown amount of input arguments that will be put together.
    
    The parameters 'current' and 'remaining' are shown here as two distinct parameters, but the user
    will only need to provide 1 delimiter and can then specify n additional strings.
*/

template <typename Argument, typename... Arguments>
std::string joinStrings (const char delimiter, const Argument& current, const Arguments&... remaining)
{
    std::string str(current);

    if constexpr (sizeof...(remaining) == 0) // This is used when there's just a single item/argument left at the end.
        return str;
    else
        return str + delimiter + joinStrings(delimiter, remaining...);
}
    
} // namespace

#endif // include guard