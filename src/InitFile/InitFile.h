/*!
    @file
    Class for reading/writing text-based configuration files ("INI files").
    
    Copyright © 2005-2014, 2021-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::InitFile
    @copybrief InitFile.h
    
    @details
    The input/output files use a "section-key-value"-system:

    @verbatim
        [Section Name]
        # Comment.
        key 1 = value_a                               # Comment.
        key 2 = value_a "Value B with spaces" value_c
        key 3 = value_a, value_b, value_c
    @endverbatim

    Some characters, like the key/value separator (by default '='), or the
    value list separator (by default ',') can be set to different symbols; see documentation.

    @note
    
    ## Issues & ideas:

    - More choices: For example whether a section or key should be added if not found; overwriting values or not...
    - iterator for (data structure)?
    - Comments are skipped/ignored -- means: When (over)writing an existing file, they are gone for good...

    History:
    2005-08-28: Initial release/project started.
    2006-02-05: Switched to SCons build-system.
    2006-02-15: readFile() now ignores/skips leading space on key=value line in a data file.
                That means the user can indent these lines via space or tabs without problems.
    2006-04-03: Moved project to its own repository.
    2013-12-05: Project will now be distributed under the terms of the "MIT license".
    2014-05-31: Switched from Visual Studio files to using CMake as a (meta) build-system.
    2014-06-01: Library is now build as Unicode by default.
    2014-08-10: Split into 'Library' and 'Test' subprojects, adjusted CMake files.
    2014-08-21: Converted to a template class (due to needed support for Unicode/wide strings) -- not complete!.
    2015-05-16: Status update: Development is on hold for now.
                For some of my other programs, I'm currently using the INI functions of the Qt framework.
    2021-02-21:
        - License change: From "MIT" to "MIT-0 (Zero Attribution)".
        - Some minor tweaks, mostly due incorporating it with the TNOT project.
        - Compiling with Clang revealed two missing 'typename'.
    2021-03-18:
        - Refreshed comments a bit, began to style them for Doxygen usage.
    2022-04-10:
        - Overhaul: Turning this into a template class was not the right choice, so I reverted that.
          The orginal reason for using a template class was to support Unicode/wide strings, but that wasn't achieved that way.
          And now std::string works just fine for UTF-8, at least for the simple actions of reading/writing text strings (to/from files).
          (The encoding/decoding to/from wide characters for the Win32-API can be handled by helper functions.)
        - Worked on misc. other outstanding issues, too.
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_AF0ED2B9_31BC_4BEE_874E_D501498BE865
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_AF0ED2B9_31BC_4BEE_874E_D501498BE865

#if !defined(__cplusplus)
    #error C++ compiler required!
#endif

#if !defined(UNICODE) || !defined(_UNICODE)
    #error UNICODE or _UNICODE must be defined!
#endif
 
#include <map>
#include <string>
#include <vector>
#include <cctype>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <filesystem>

using std::vector;
using std::map;

namespace NiftyOddity::Toolkit::InitFile
{

/*!
    Represents the data of a configuration file and offers methods to work with it.
        
    The usual workflow is: Create an InitFile object, let it open an existing key/value-based file, let it read
    the data, modify the data, and finally write it out again to a file on-disk.
*/
class InitFile
{
    public:
        InitFile (char const comment_token = '#');
        InitFile (std::string const& filename, char const comment_token = '#');
        InitFile (std::filesystem::path filepath, char const comment_token = '#');
        InitFile (const InitFile&, char const comment_token = '#');
        ~InitFile ();
        
        bool readFile  (std::string const& filename, char const keyvalue_separator = '=');
        bool readFile  (std::ifstream infile, char const keyvalue_separator = '=');
        bool writeFile (std::string filename = "out.ini", char const keyvalue_separator = '=');

        bool isSection (std::string const& sec);
        bool isKey     (std::string const& key);
        bool isKey     (std::string const& sec, std::string const& key);      
        
        bool addSection    (std::string const& sec);
        void deleteSection (std::string const& sec);
        bool renameSection (std::string const& sec, std::string const& new_name);
        
        bool addKey    (std::string const& sec, std::string const& key, std::string const& val);
        bool deleteKey (std::string const& sec, std::string const& key);

        bool        setValue (std::string const& sec, std::string const& key, std::string const& val);
        std::string getValue (std::string const& sec, std::string const& key);
        
        std::vector<std::string> splitValue (std::string input, char const separator = ',');
        std::string              joinValue  (std::vector<std::string>, char const separator = ',') const;
        
        InitFile& operator=  (const InitFile& i);
        bool      operator== (const InitFile& right) const;
        bool      operator!= (const InitFile& right) const;
    
    private:
        struct section
        {
            std::string name;
            std::map<std::string, std::string> key_val;
        };
        
        std::vector<section*> configdata;

        const char comment_token;
        
        std::vector<section*>::iterator              findSection (std::string const& sec);
        std::map<std::string, std::string>::iterator findKey     (std::string const& sec, std::string const& key);
        std::map<std::string, std::string>::iterator findKey     (std::vector<section*>::iterator sec_pos, std::string const& key);
        
        std::string stripSpace (std::string input, bool strip_all);
};


//! Constructor.
//
InitFile::InitFile (char const comment_token)
    : comment_token(comment_token)
{
}


/*!
    Constructor.

    @param filename The file which should be opened and read.

    @exception May throw a std::runtime_error exception due to delegating the work to another constructor.
*/
InitFile::InitFile (std::string const& filename, char const comment_token)
    : InitFile (std::filesystem::path(filename), comment_token)
{
    // Delegates its work to another class constructor.
}


/*!
    Constructor.

    @param filepath The path to the file that should be opened and read.

    @exception Throws a std::runtime_error exception if the filepath is not valid or not a regular file.
*/

InitFile::InitFile (std::filesystem::path filepath, char const comment_token)
    : comment_token(comment_token)
{
    std::error_code ec;

    if (std::filesystem::exists(filepath) && std::filesystem::is_regular_file(filepath, ec))
    {
        readFile(filepath.string());
    }
    else
    {
        throw std::runtime_error(std::string("Filepath " + filepath.string() + " could not be found or is not a file."));
    }
}


//! Copy constructor.
//
InitFile::InitFile (const InitFile& original_object, char const comment_token)
    : comment_token(comment_token)
{
    for (vector<section*>::const_iterator pos = original_object.configdata.begin(); pos != original_object.configdata.end(); ++pos)
    {
        section* s = new section;
        
        s->name = (*pos)->name;
        
        for (map<std::string, std::string>::const_iterator kv_pos = (*pos)->key_val.begin(); kv_pos != (*pos)->key_val.end(); ++kv_pos)
        {
            std::pair<std::string, std::string> p;
            p.first  = kv_pos->first;
            p.second = kv_pos->second;
            s->key_val.insert(p);
        }   
        configdata.push_back(s);
    }
}


//! Destructor.
// 
InitFile::~InitFile ()
{
    for (vector<section*>::iterator pos = configdata.begin(); pos != configdata.end(); ++pos)
    {
        delete *pos;
    }
}


//! Assignment operator.
// 
InitFile& InitFile::operator= (const InitFile& right)
{
    if (this != &right)
    {
        // Clear the destination (left side) object.
        for (vector<section*>::iterator left = configdata.begin(); left != configdata.end(); left++)
        {
            delete *left;
        }
        configdata.clear();
    
        // Create a new section for each entry and fill it with the values from the source (right side) object.
        for (vector<section*>::const_iterator pos = right.configdata.begin(); pos != right.configdata.end(); ++pos)
        {
            section* s = new section;
            s->name = (*pos)->name;
        
            for (map<std::string, std::string>::const_iterator kv_pos = (*pos)->key_val.begin(); kv_pos != (*pos)->key_val.end(); ++kv_pos)
            {
                std::pair<std::string, std::string> p;
                p.first  = kv_pos->first;
                p.second = kv_pos->second;
                s->key_val.insert(p);
            }   
            configdata.push_back(s);
        }
    }
    return *this;
}


/*!
    Equality operator.
  
    Comparison of configdata and its sections only! Order is considered (method bails out as soon as there is a difference).
*/
bool InitFile::operator== (const InitFile& right) const
{
    if (this == &right)
    {
        return true;
    }
    else
    {
        if (configdata.size() != right.configdata.size())
        {
            return false;
        }
        else
        {
            vector<InitFile::section*>::const_iterator pl = configdata.begin();
            vector<InitFile::section*>::const_iterator pr = right.configdata.begin();
            
            while (pl != configdata.end())
            {
                if ((*pl)->name != (*pr)->name)
                {
                    return false;
                }
                else
                {
                    map<std::string, std::string>::const_iterator pml = (*pl)->key_val.begin();
                    map<std::string, std::string>::const_iterator pmr = (*pr)->key_val.begin();
    
                    while (pml != (*pl)->key_val.end())
                    {
                        if (pml->first != pmr->first)
                        {
                            return false;
                        }
                        else
                        {
                            if (pml->second != pmr->second)
                                return false;
                            else
                            {
                                ++pml; ++pmr;
                            }
                        }
                    }
                }
                ++pl; ++pr;
            }
        }   
        return true;
    }
}


//! Inequality operator.
// 
bool InitFile::operator!= (const InitFile& a) const
{
    return !(*this == a);
}


//! Check whether a section already exists or not.
//
bool InitFile::isSection (std::string const& sec)
{
    vector<InitFile::section*>::iterator pos;
    
    for (pos = configdata.begin(); pos != configdata.end(); ++pos)
    {
        if ((*pos)->name == sec)
            return true;
        else
            continue;
    }
    return false;
}


//! Check whether a key in a certain section already exists or not.
// 
bool InitFile::isKey (std::string const& sec, std::string const& key)
{
    if (isSection(sec))
    {
        vector<InitFile::section*>::iterator sec_pos = findSection(sec);
        map<std::string, std::string>::iterator pos;
    
        for (pos = (*sec_pos)->key_val.begin(); pos != (*sec_pos)->key_val.end(); ++pos)
        {
            if (pos->first == key)
                return true;
            else
                continue;
        }
        return false;
    }
    else
    {
        return false;
    }
}


/*!
    Check whether a key exists at all (regardless in which section).

    @attention Returns at the first match; that means that keys with the same name in different sections will not be found!
*/
bool InitFile::isKey (std::string const& key)
{
    vector<InitFile::section*>::iterator sec_pos;
    map<std::string, std::string>::iterator pos;
    
    for (sec_pos = configdata.begin(); sec_pos != configdata.end(); ++sec_pos)
    {
        for (pos = (*sec_pos)->key_val.begin(); pos != (*sec_pos)->key_val.end(); ++pos)
        {
            if (pos->first == key)
                return true;
            else
                continue;
        }   
    }
    return false;
}


/*!
    Find position of a section.

    Returns the position of an existing section (returns the .end()-position on failure)
*/
vector<InitFile::section*>::iterator InitFile::findSection (std::string const& sec)
{
    vector<InitFile::section*>::iterator pos;
    
    for (pos = configdata.begin(); pos != configdata.end(); ++pos)
    {
        if ((*pos)->name == sec)
            break;
        else
            continue;
    }
    return pos;
}


/*!
    Find position of a key.

    Returns the position of an existing key in an existing section (returns the .end()-position on failure).
*/
map<std::string, std::string>::iterator InitFile::findKey (std::string const& sec, std::string const& key)
{
    vector<InitFile::section*>::iterator sec_pos = findSection(sec);
    map<std::string, std::string>::iterator pos;
    
    for (pos = (*sec_pos)->key_val.begin(); pos != (*sec_pos)->key_val.end(); ++pos)
    {
        if (pos->first == key)
            return pos;
        else
            continue;
    }
    return pos;
}


/*!
    Find position of a key.
 
    @note Requires a valid section position.

    @return the position of an existing key in an existing section (returns the .end()-position on failure).
*/
map<std::string, std::string>::iterator InitFile::findKey (vector<InitFile::section*>::iterator sec_pos, std::string const& key)
{
    map<std::string, std::string>::iterator pos;
    
    for (pos = (*sec_pos)->key_val.begin(); pos != (*sec_pos)->key_val.end(); ++pos)
    {
        if (pos->first == key)
            return pos;
        else
            continue;
    }
    return pos;
}




/*!
    Set a new value for an existing key.
    
    @param s The section.
    @param k The key.
    @param v The new value for the key.
*/
bool InitFile::setValue (std::string const& sec, std::string const& key, std::string const& val)
{
    vector<InitFile::section*>::iterator sec_pos = findSection(sec);
    
    if (sec_pos == configdata.end())
        return false;
    else
    {
        for (map<std::string, std::string>::iterator kv_pos = (*sec_pos)->key_val.begin(); kv_pos != (*sec_pos)->key_val.end(); ++kv_pos)
        {
            if (kv_pos->first == key)
            {
                kv_pos->second = val;
                return true;
            }
            else
                continue;
        }
        return false;
    }
}


/*!
    Get the value for an existing key in an existing section.
    
    @return The value of the key, or an empty string (on failure to get a value).
*/
std::string InitFile::getValue (std::string const& sec, std::string const& key)
{
    vector<InitFile::section*>::iterator sec_pos = findSection(sec);
    
    if (sec_pos != configdata.end())
    {
        for (map<std::string, std::string>::iterator kv_pos = (*sec_pos)->key_val.begin(); kv_pos != (*sec_pos)->key_val.end(); ++kv_pos)
        {
            if (kv_pos->first == key) { return kv_pos->second; }
            else                      { continue; }
        }
    }
    
    return "";
}


//! Add a new key to an existing section.
//
bool InitFile::addKey (std::string const& sec, std::string const& key, std::string const& val)
{
    vector<InitFile::section*>::iterator s = findSection(sec);
    
    if (s == configdata.end())
    {
        return false;
    }
    else
    {
        std::pair<std::string, std::string> p;
        p.first  = key;
        p.second = val;
        (*s)->key_val.insert(p);
        return true;
    }
}


/*!
    Delete a key from an existing section.

    @note Note from 'The C++StdLib'/Nicolai M. Josuttis, Chapter 6.6:
    There is a big danger that you will remove an element to which you iterator is refering. [...]
    It was a design decision, not to provide [returning the value of the follwing element] [...]
    Note that pos++ increments pos [...] but yields a copy of its original value. [...]"
*/
bool InitFile::deleteKey (std::string const& sec, std::string const& key)
{
    vector<InitFile::section*>::iterator sec_pos = findSection(sec);
    
    if (sec_pos == configdata.end())
    {
        return false;
    }
    else
    {
        map<std::string, std::string>::iterator kv_pos = findKey(sec_pos, key);
        
        for (kv_pos = (*sec_pos)->key_val.begin(); kv_pos != (*sec_pos)->key_val.end(); ++kv_pos)
        {
            if (kv_pos->first == key)
            {
                (*sec_pos)->key_val.erase(kv_pos++); // see comment above.
                return true;
            }
            else
            {
                continue;
            }
        }
    }
    return false;
}


//! Add a new section to the container.
//
bool InitFile::addSection (std::string const& sec)
{
    if (!isSection(sec))
    {
        section* s = new section;
        s->name = sec;
        configdata.push_back(s);
        return true;
    }
    else
    {
        return false;
    }
}


//! Delete a section from the container.
//
void InitFile::deleteSection (std::string const& sec)
{
    vector<InitFile::section*>::iterator pos;
    pos = findSection(sec);
    
    if (pos != configdata.end())
    {
        delete *pos;
        configdata.erase(pos);
    }
}


//! Set a new name for an existing section.
//
bool InitFile::renameSection (std::string const& sec, std::string const& new_name)
{
    vector<InitFile::section*>::iterator pos = findSection(sec);
    
    if (pos == configdata.end())
        return false;
    else
    {
        (*pos)->name = new_name;
        return true;
    }
}


/*!
    Split a given string s into chunks.

    A value-string can be split when each component is separated by specified character
    (be it a comma, semicolor or space...).
    
    A component itself can contain space, if it is enclosed in double-quotation-marks.
    This one will produce five chunks: value-1, "string with space" and more, values_2
    
    @param input     The input string; e.g. the value part of a key/value pair.
    @param separator The delimiter symbol; a default value is given, but other characters are possible (e.g. space: ' ').
    
    @return A vector of strings, each element of that vector represents a chunk of the splitted input value.
*/
vector<std::string> InitFile::splitValue (std::string input, char const separator)
{
    std::string::iterator end = input.end();
    
    vector<std::string> output;
    std::string temp;
    
    for (std::string::iterator pos = input.begin(); pos != end; ++pos)
    {
        if (isspace(*pos) || *pos == separator)
        {
            continue;
        }
        else
        {
            if (*pos == '"' && (pos+1) != end)
            {
                ++pos;
                
                while (*pos != '"' && (pos+1) != end)
                {
                    temp += *pos;
                    ++pos;
                }
                
                output.push_back(temp);
                temp.clear();
            }       
            else
            {
                while (*pos != separator && (pos+1) != end)
                {
                    temp += *pos;
                    ++pos;
                }
                
                // The previous loop exits one character before the end.
                if (*pos != separator)
                    temp += *pos;

                output.push_back(temp);
                temp.clear();
            }
        }
    }

    return output;
}


/*!
    Join a vector of strings into one single string, separated by the given symbol.
    
    @param input     The input string; e.g. the value part of a key/value pair.
    @param separator The delimiter symbol; a default value is given, but other characters are possible (e.g. space: ' ').

    @return A single string that combines all input components, separated by the given symbol.
*/
std::string InitFile::joinValue (vector<std::string> input, char const separator) const
{
    std::string s; // As far as I know: Standard guarantees the string to be initialized with 'zero'/non-garbage value.
    
    if (input.empty())
    {
        return s;
    }
    else
    {
        for (vector<std::string>::iterator pos = input.begin(); pos != input.end(); ++pos)
        {
            if (pos == (input.end()-1) || input.size() == 1)
                s.append(*pos);
            else
                s.append(*pos + std::string(1, separator));
        }   
        return s;
    }
}


/*!
    Read the data from an on-disk-file into an in-memory-structure.

    The file data must be built after the common INI model, for example:
    
    @verbatim 
        [Section Name]
        # Comment.
        key 1 = value # Comment.
    @endverbatim 
    
    The value part can consists of multiple parts, separated by space and/or comma.  
    A value with space must be enclosed by double-quotation marks ("x y z").
    
    @param filename The name of/path to the the file that should be read.
    
    @return A boolean value of `true` when everything went smooth; else returns `false`.
*/
bool InitFile::readFile (std::string const& filename, char const keyvalue_separator)
{
    return readFile(std::ifstream(filename), keyvalue_separator);
}


/*!
    Read the data from a file-stream into an in-memory-structure.
*/
bool InitFile::readFile (std::ifstream infile, char const keyvalue_separator)
{
    std::string line, key, value;
    bool is_first_run = true; // To control that the first section is correctly put in the container (see below).

    section* s = new section;
    
    if (!s)
        return false;

    if (!infile)
    {
        return false;
    }
    else
    {       
        while (infile.good())
        {
            std::string* kv_target = &key;
            key.clear(); value.clear(); line.clear();
            std::getline(infile, line);

            bool isKeyDone = false;
                // This is to prevent that the key/value separator will be replaced in the value (due to mode switching):
                // For example: If the key separator is ':' and one value is "C:\path", when writing back, the value was then "C:\path"
                // (missing colon since it was the same as the key/value separator character).

            bool isEnclosedString = false;
                // This will prevent that a line would be prematurely cut-off if the comment character appears in a value string:
                // Will help with this: key = "Some Value #1" # Real comment.
                // But not here (by design): key = Value #1 # Real comment.

            if (line[0] == comment_token || line.empty())
            {
                continue;
            }
            else
            {
                for (std::string::iterator pos = line.begin(); pos != line.end(); ++pos)
                {
                    if (*pos == '"')
                        isEnclosedString = !isEnclosedString;
                    
                    if (*pos == comment_token && !isEnclosedString)
                        break;
                    
                    if (*pos == '[')        // Handle sections.
                    {
                        // If it's not the first section, put the previously read data into the container and start with a new section.
                        // NOTE: This 'flag'-solution is somewhat ugly, I think -- but at that day, no other approach worked correctly for me.
                        if (!is_first_run)
                        {
                            configdata.push_back(s);
                            s = new section; // FIXME: testing for success or failure.
                        }
                        else
                        {
                            is_first_run = false;
                        }

                        s->name.clear();
                        ++pos;

                        while (*pos != ']')
                        {
                            s->name += *pos;
                            ++pos;
                        }
                    }
                    else                    // Handle keys and values.
                    {
                        // Parse the keys and values; toogle the target buffer to 'value' when done with 'key'.
                        if (*pos == keyvalue_separator && !isKeyDone)
                        {
                            kv_target = &value;
                            isKeyDone = true;
                        }
                        else
                        {
                            *kv_target += *pos;
                        }
                    }
                }

                if (!key.empty() && !value.empty())
                {
                    key = stripSpace(key, false);
                    value = stripSpace(value, false);

                    std::pair<std::string, std::string> p;
                    p.first  = key;
                    p.second = value;                   
                    s->key_val.insert(p);
                }
            }
        }

        configdata.push_back(s);
    }

    return true;
}


/*!
    Write the data from an in-memory-structure into an on-disk-file.

    @todo If that file already exists, it will be overwritten? Confirm this.

    // std::error_code ec;
    // if (std::filesystem::exists(filepath) && std::filesystem::is_regular_file(filepath, ec))
    
    @param The name/path of the file to which it should be saved.
*/
bool InitFile::writeFile (std::string filename, char const keyvalue_separator)
{
    std::ofstream outfile (filename);

    if (!outfile)
    {
        return false;
    }
    else
    {
        for (vector<section*>::iterator pos = configdata.begin(); pos != configdata.end(); ++pos)
        {
            outfile << "[" << (*pos)->name << "]" << std::endl;

            for (map<std::string, std::string>::iterator kv_pos = (*pos)->key_val.begin(); kv_pos != (*pos)->key_val.end(); ++kv_pos)
            {
                outfile <<  kv_pos->first << keyvalue_separator << kv_pos->second << std::endl;
            }
        }
        return true;
    }
}


/*!
    Strip leading and trailing space in a string.
    
    @param input     The string that should be cleared.
    @param strip_all Set this to true if the spaces between the characters/words should be erased, too.
    
    @return A string without the spaces.
    
    @bug For a spaces-only string (" ", "   ") the result is still a one-space string (" ").
*/
std::string InitFile::stripSpace (std::string input, bool strip_all)
{
    if (input.empty())
    {
        return input;
    }
    else
    {
        std::string output;

        std::string::iterator pos_beg = input.begin();
        std::string::iterator pos_end = input.end();     
        
        while (!isgraph(*pos_beg))
        {
            if (pos_beg != pos_end)
                ++pos_beg;
            else
                break;
        }
        
        while (!isgraph(*(pos_end-1)))
        {
            if (pos_beg != pos_end)
                --pos_end;
            else
                break;
        }
        
        if (strip_all)
        {
            // Remove also the spaces between the characters/words.
            for (std::string::iterator pos_cur = pos_beg; pos_cur != pos_end; ++pos_cur)
            {
                 if (isspace(*pos_cur))
                 {
                    ++pos_cur;
                    input.erase(pos_cur-1);
                    --pos_end;
                 }
             }
        }
        
        while (pos_beg != pos_end)
        {
            output += *pos_beg;
            ++pos_beg;
        }
        
        return output;
    }   
}

} // namespace

#endif // include guard
