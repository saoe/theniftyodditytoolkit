//
// Test/Demo/Example for code in the 'CLI::Utilities' section.
//

#include <iostream>
#include "Utilities.hpp"

namespace util = NiftyOddity::Toolkit::CLI::Utilities;

int main (int argc, char* argv[])
{
    std::cout << "\n________ Spinning busy cursor ________" << std::endl;
    
    int max = 33;
    
    for (int i = 0; i <= max; i++)
    {
        std::cout << "\rTesting the busy cursor ";
        util::showBusyCursor();
    }
    std::cout << '\n';
    
    std::cout << "\n________ Progress bar ________" << std::endl;
    
    for (int i = 0; i <= max; i++)
    {
        int percent = i*100/max;
        util::showProgressBar(percent);
    }
        
    return 0;
}
