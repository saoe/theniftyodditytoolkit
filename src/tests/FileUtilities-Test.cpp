// Test/Demo/Example code for 'FileUtilities'.

#include "FileUtilities.h"
#include <iostream>

namespace tnotf = NiftyOddity::Toolkit::FileUtilities;

int main (int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << "\nMissing argument: Specify a path to a file!\n";
        return 0;
    }
    
    try
    {
        std::cout << "\n\n===================================================================\n";
        std::cout << "Testing checksum calculation of a file\n";

        std::cout << '\n';
        std::cout << "Filepath : " << argv[1] << '\n';
        std::cout << "Algorithm: SHA-256 (hardwired)\n";
        std::cout << "Checksum : " << tnotf::calculateFileChecksum(argv[1], tnotf::HashAlgorithm::SHA256) << '\n';
        
        std::cout << "\n\n===================================================================\n";
        std::cout << "---- Version Info of the file: ----\n";
        
        tnotf::FileVersionInfo vi_exe = tnotf::getFileVersionInfo(argv[1]);
    
        std::cout << "File Major: " << vi_exe.FileMajorVersion << "\n";
        std::cout << "File Minor: " << vi_exe.FileMinorVersion << "\n";
        std::cout << "File Patch: " << vi_exe.FilePatchVersion << "\n";
        std::cout << "File Rev. : " << vi_exe.FileRevisionVersion << "\n";

        std::cout << "Product File Major: " << vi_exe.ProductMajorVersion << "\n";
        std::cout << "Product File Minor: " << vi_exe.ProductMinorVersion << "\n";
        std::cout << "Product File Patch: " << vi_exe.ProductPatchVersion << "\n";
        std::cout << "Product File Rev. : " << vi_exe.ProductRevisionVersion << "\n";
    }
    catch (std::exception e)
    {
        std::cout << e.what();
    }
    
    return 0;
}