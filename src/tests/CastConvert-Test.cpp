// Test/Demo/Example code for 'CLIUtilities'.

#include <iostream>
#include "CastConvert.h"

// Sample Text: Приве́т नमस्ते שָׁלוֹם (segment three is Right-To-Left).
std::string sample_text_s   = "\xD0\x9F\xD1\x80\xD0\xB8\xD0\xB2\xD0\xB5\xCC\x81\xD1\x82\x20\xE0\xA4\xA8\xE0\xA4\xAE\xE0\xA4\xB8\xE0\xA5\x8D\xE0\xA4\xA4\xE0\xA5\x87\x20\xD7\xA9\xD6\xB8\xD7\x81\xD7\x9C\xD7\x95\xD6\xB9\xD7\x9D";
const char sample_text_c [] = "\xD0\x9F\xD1\x80\xD0\xB8\xD0\xB2\xD0\xB5\xCC\x81\xD1\x82\x20\xE0\xA4\xA8\xE0\xA4\xAE\xE0\xA4\xB8\xE0\xA5\x8D\xE0\xA4\xA4\xE0\xA5\x87\x20\xD7\xA9\xD6\xB8\xD7\x81\xD7\x9C\xD7\x95\xD6\xB9\xD7\x9D";

namespace cc = NiftyOddity::Toolkit::CastConvert;

int main (int argc, char* argv[])
{
    std::cout << "--- Testing UTF-8 <-> UTF-16 conversion: Writing to file(s)... ---" << std::endl;
    
    sample_text_s.append("\xE2\x80\x8E"); // Inserts a LRM <https://en.wikipedia.org/wiki/Left-to-right_mark> after the arabic right-to-left text in the sample text.
    sample_text_s.append(" 2.txt");       // Otherwise, the '2' will appear in the middle of the string (but '.txt' would still be appended at the right; exeception rule due punctuation?).
    CreateFileW((LPCWSTR)cc::widenString(sample_text_s).c_str(), (GENERIC_READ | GENERIC_WRITE), FILE_SHARE_DELETE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    CreateFileW((LPCWSTR)cc::widenString(sample_text_c).c_str(), (GENERIC_READ | GENERIC_WRITE), FILE_SHARE_DELETE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    
    // ---------------------------------------------------------------------------------------------
    
    std::cout << "--- Testing translateSecondsToHandyText() ---" << std::endl;
    
    std::string setpoint_value, actual_value;

    actual_value = cc::translateSecondsToHandyText(5);
    setpoint_value = "5s";
    std::cout << "Test input: 5 seconds\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(10);
    setpoint_value = "10s";
    std::cout << "Test input: 10 seconds\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}
    
    actual_value = cc::translateSecondsToHandyText(60);
    setpoint_value = "1m:00s";
    std::cout << "Test input: 60 seconds (= 1 minute)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}
    
    actual_value = cc::translateSecondsToHandyText(185);
    setpoint_value = "3m:05s";
    std::cout << "Test input: 185 seconds (3 minutes + 5 seconds)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(3600);
    setpoint_value = "1h:00m:00s";
    std::cout << "Test input: 3'600 seconds (1 hour)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(3700);
    setpoint_value = "1h:01m:40s";
    std::cout << "Test input: 3'700 seconds (1 hour + 1 minute + 40 seconds)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(86400);
    setpoint_value = "1d:00h:00m:00s";
    std::cout << "Test input: 86'400 seconds (24 hours/1 day)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(90061);
    setpoint_value = "1d:01h:01m:01s";
    std::cout << "Test input: 90'061 seconds (1 day (24 hours) + 1 hour + 1 minute + 1 second)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    // 1 week = 7 days = 168 hours = 10,080 minutes = 604,800 seconds <https://en.wikipedia.org/wiki/Week>.
    actual_value = cc::translateSecondsToHandyText(604800);
    setpoint_value = "1w:0d:00h:00m:00s";
    std::cout << "Test input: 604'800 seconds (1 week (7 days))\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    actual_value = cc::translateSecondsToHandyText(735910);
    setpoint_value = "1w:1d:12h:25m:10s"; // Not calculated, just copied from the result of this code!
    std::cout << "Test input: 735'910 seconds (1 week + ...)\n" <<
        "  Setpoint value: " << setpoint_value << '\n' <<
        "  Actual value  : " << actual_value << "\n";
    if (setpoint_value == actual_value) { std::cout << "  -> OK\n\n"; }  else { std::cout << "  -> Failure!\n\n";}

    return 0;
}
