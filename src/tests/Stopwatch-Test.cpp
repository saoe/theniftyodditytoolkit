// Test/Demo/Example code for 'Stopwatch'.

#include "Stopwatch.hpp"
#include <iostream>
#include <thread>

namespace tnot = NiftyOddity::Toolkit;
using tnot::Stopwatch;

void printResult (tnot::Stopwatch::Result e)
{
    std::cout << "=========================\n";
    std::cout << "Elapsed (active) time:\n";
    std::cout << "    Hours............: " << e.hours << '\n';
    std::cout << "    Minutes..........: " << e.minutes << '\n';
    std::cout << "    Seconds..........: " << e.seconds << '\n';
    std::cout << "    Milliseconds.....: " << e.milliseconds << '\n';
    std::cout << "    Microseconds.....: " << e.microseconds << '\n';
    std::cout << "    Laps.............: " << e.lap_count << '\n';
    for (auto i : e.laps_in_ms)
    {
        std::cout << "    - Duration (ms)..: " << i << '\n';
    }
    std::cout << "    Mean (ms)........: " << e.mean_in_ms << '\n';
    std::cout << "    Median (ms)......: " << e.median_in_ms << '\n';
    std::cout << "    Midrange (ms)....: " << e.midrange_in_ms << '\n';
}


int main (int argc, char* argv[])
{
    std::cout << "Testing Stopwatch...\n";
    
    using std::this_thread::sleep_for;
    
    Stopwatch s;

    std::cout << "\n---- Test #1: Simple ------------\n";
    s.start();
    std::cout << "Work for 2 seconds...\n";
    sleep_for(std::chrono::seconds(2));
    s.stop();
    printResult(s.result());
    
    std::cout << "\n---- Test #2: With pauses ------------\n";
    s.start();
    std::cout << "Work for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.pause();
    std::cout << "Pause for 2 seconds...\n";
    sleep_for(std::chrono::seconds(2));
    s.resume();
    std::cout << "Work for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.pause();
    std::cout << "Pause for 2 seconds...\n";
    sleep_for(std::chrono::seconds(2));
    s.resume();
    std::cout << "Work for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.stop();
    printResult(s.result());

    std::cout << "\n---- Test #3: With split times ------------\n";
    s.start();
    std::cout << "Work for 1 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(1));
    s.split();
    std::cout << "Work for 2 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(2));
    s.split();
    std::cout << "Work for 3 seconds, then stop...\n";
    sleep_for(std::chrono::seconds(3));
    s.stop();
    printResult(s.result());

    std::cout << "\n---- Test #4a: With odd split times & pauses ------------\n";
    s.start();
    std::cout << "Work for 1 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(1));
    s.split();
    s.pause();
    std::cout << "Pause for 2 seconds...\n";
    sleep_for(std::chrono::seconds(2));
    s.resume();
    std::cout << "Work for 2 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(2));
    s.split();
    s.pause();
    std::cout << "Pause for 2 seconds...\n";
    sleep_for(std::chrono::seconds(2));
    s.resume();
    std::cout << "Work for 3 seconds, then stop...\n";
    sleep_for(std::chrono::seconds(3));
    s.stop();
    printResult(s.result());

    std::cout << "\n---- Test #4b: With even split times & pauses------------\n";
    s.start();
    std::cout << "Work for 1 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(1));
    s.split();
    s.pause();
    std::cout << "Pause for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.resume();
    std::cout << "Work for 2 seconds, then measure split time...\n";
    sleep_for(std::chrono::seconds(2));
    s.split();
    s.pause();
    std::cout << "Pause for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.resume();
    std::cout << "Work for 3 seconds, then stop...\n";
    sleep_for(std::chrono::seconds(3));
    s.split();
    s.pause();
    std::cout << "Pause for 1 seconds...\n";
    sleep_for(std::chrono::seconds(1));
    s.resume();
    std::cout << "Work for 4 seconds, then stop...\n";
    sleep_for(std::chrono::seconds(4));
    s.stop();
    printResult(s.result());
    
    return 0;
}