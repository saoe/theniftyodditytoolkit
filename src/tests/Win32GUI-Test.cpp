// Test/Demo/Example code for 'CLIUtilities'.

#include <iostream>
#include "Win32GUI.h"

namespace gui = NiftyOddity::Toolkit::Win32GUI;

int main (int argc, char* argv[])
{
    std::cout << "--- Test of getErrorDescription() - expected result: An error message ---\n\
    [Message in EN] \"0x0002: The system cannot find the file specified.\"\n\
    [Message in DE] \"0x0002: Das System kann die angegebene Datei nicht finden.\"\n\n";
    
    GetFileAttributes(L"C:\\non-existing-file.txt");
    std::cout << gui::getErrorDescription();

    //std::cout << "--- Test for centerWindow() not yet available! ---"

    return 0;
}
