// Test/Demo/Example code for 'KeyboardLED'.

#include <iostream>
#include "KeyboardLED.h"

namespace led = NiftyOddity::Toolkit::KeyboardLED;

int main (int argc, char* argv[])
{
	led::KeyboardLED kl1;
	
    std::cout << "+++ Play pattern #1 +++\n";
    led::Pattern pattern1;
    pattern1.play_count = 1;
    pattern1.pause_between_patterns_in_milliseconds = 100;
    
    led::LightControl l1(led::ChangeStateTo::NumLockOff_CapsLockOff_SrcollLockOff);
    led::LightControl l2(led::ChangeStateTo::NumLockOn_CapsLockOff_SrcollLockOff);
    led::LightControl l3(led::ChangeStateTo::NumLockOff_CapsLockOn_SrcollLockOff);
    led::LightControl l4(led::ChangeStateTo::NumLockOff_CapsLockOff_SrcollLockOn);
    pattern1.states = { l1, l2, l3, l4 };

	kl1.play(pattern1);
    
    std::cout << "+++ Play pattern #2 with a new KeyboardLED object instance +++\n";
    led::Pattern pattern2;
    pattern2.play_count = 2;
    led::LightControl m1(led::ChangeStateTo::NumLockOff_CapsLockOn_SrcollLockOff);
    led::LightControl m2(led::ChangeStateTo::NumLockOn_CapsLockOff_SrcollLockOn);
    led::LightControl m3(led::ChangeStateTo::NumLockOff_CapsLockOn_SrcollLockOff);
    pattern2.states = { m1, m2, m3 };
    led::KeyboardLED kl2(pattern2);
    kl2.play();
    
    std::cout << "+++ Invert the LEDs +++\n";
    kl2.invert();
    Sleep(2000);
    
    std::cout << "+++ Flash all LEDs +++\n";
    kl2.reset();
    kl2.flash(3);
}
