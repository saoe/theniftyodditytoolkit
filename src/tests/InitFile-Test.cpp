/*
Test/Demo/Example: Working with initialization/configuration file/data
    
Copyright © 2005-2014, 2021-2022 Sascha Offe <so@saoe.net>
SPDX-License-Identifier: MIT-0

--- INPUT: data.ini ------------------------------------

[S1] # Comment.
1 A = "C:\path to\somewhere"
1 B = 1.1
1 C = xyz

# Comment.
[S2]
2 A = Wert-2 mit Leerzeichen aber ohne Anfuehrungszeichen # Comment.
2 B = 21

[S3]
3 A = value1, value2, value3
3 B = value, "A string", value
3 C = "Value and space #1", "Value and space #2" # Comment. Note the character in the string!

[S4] # UTF-8 stuff
4 A = value1, value2, value3
4 B = value, "ሰማይ አይታረስ ንጉሥ አይከሰስ።", value, "Десятую", value, "핐 핑 핒 핓 핔 핕 핖 핗 하 학 핚 핛 한 핝 핞 핟 할"
4 C = "Value and space 1", "Value and space 2" # Comment.

---------------------------------------------------------
*/

#include <iostream>
#include <sstream>
#include "InitFile.h"

#if !defined(UNICODE) || !defined (_UNICODE)
    #error Fatal error: UNICODE/_UNICODE must be defined!
#endif

using std::endl;
namespace tnotif = NiftyOddity::Toolkit::InitFile;

int main (int argc, char* argv[])
{
    std::cout << "\n--- Creating objects on the heap..." << endl;
    
    tnotif::InitFile* init_obj_1 = new tnotif::InitFile();
    
    std::cout << "\n--- Trying to open and read file(s)..." << endl;

    if (argc == 2)
    {
        // Convert char** to std::string to std::string for readFile().
        std::string input(argv[1]);
        std::stringstream ss;
        ss << input.data();
        std::string winput = ss.str();
        
        if (init_obj_1->readFile(winput))
            std::cout << "Opened file." << endl;
        else
            exit(1);
    }
    else
    {
        std::cout << "Error: Filename missing!" << endl;
        exit(1);
    }


    std::cout << "\n--- Testing Copy constructor..." << endl;
    
    tnotif::InitFile* init_obj_2 = new tnotif::InitFile(*init_obj_1);
    
    if (*init_obj_1 == *init_obj_2)
        std::cout << "Twins!" << endl;


    std::cout << "\n--- Testing Assignment operator..." << endl;
    
    tnotif::InitFile init_obj_3, init_obj_4;
    init_obj_3 = *init_obj_1;
    init_obj_4 = init_obj_3;
     
     
    std::cout << "\n--- Testing is*() and getValue..." << endl; 
    
    if (init_obj_2->isSection("S1"))
    {
        if (init_obj_2->isKey("1 A"))
            std::cout << init_obj_3.getValue("S1", "1 A") << endl;
        
        if (init_obj_2->isKey("1 B"))
            std::cout << init_obj_3.getValue("S1", "1 B") << endl;
        
        if (init_obj_2->isKey("1 C"))
            std::cout << init_obj_3.getValue("S1", "1 C") << endl;
    }
     
    if (init_obj_2->isSection("S2"))
    {
        if (init_obj_2->isKey("S2", "2 A"))
            std::cout << init_obj_3.getValue("S2", "2 A") << endl;
        
        if (init_obj_2->isKey("S2", "2 B"))
            std::cout << init_obj_3.getValue("S2", "2 B") << endl;
        
        if (init_obj_2->isKey("S2", "2 C"))
            std::cout << init_obj_3.getValue("S2", "2 C") << endl;
    }

    if (init_obj_2->isSection("S3"))
    {       
        if (init_obj_2->isKey("S3", "3 A"))
            std::cout << init_obj_3.getValue("S3", "3 A") << endl;
        
        if (init_obj_2->isKey("S3", "3 B"))
            std::cout << init_obj_3.getValue("S3", "3 B") << endl;
        
        if (init_obj_2->isKey("S3", "3 C"))
            std::cout << init_obj_3.getValue("S3", "3 C") << endl;
    }

    if (init_obj_2->isSection("S4"))
    {       
        if (init_obj_2->isKey("S4", "4 A"))
            std::cout << init_obj_3.getValue("S4", "4 A") << endl;
        
        if (init_obj_2->isKey("S4", "4 B"))
            std::cout << init_obj_3.getValue("S4", "4 B") << endl;
        
        if (init_obj_2->isKey("S4", "4 C"))
            std::cout << init_obj_3.getValue("S4", "4 C") << endl;
    }
    
    std::cout << "\n--- Testing get-/setValue()..." << endl;
    
    std::cout << "Old value: " << init_obj_3.getValue("S2", "2 B") << endl;
    init_obj_3.setValue("S2", "2 B", "42");
    std::cout << "New value: " << init_obj_3.getValue("S2", "2 B") << endl;

    
    std::cout << "\n--- Testing add-/delete-/renameSection()..." << endl;
    
    if (init_obj_1->addSection("S-Z"))
        std::cout << "OK" << endl;
    
    init_obj_1->deleteSection("S1");

    if (init_obj_1->renameSection("S2", "SX"))
        std::cout << "OK" << endl;    

    
    std::cout << "\n--- Testing add-/deleteKey()..." << endl;

    if (init_obj_2->addKey("S1", "1 D", "Foo!"))
        std::cout << "OK" << endl;

    if (init_obj_2->deleteKey("S1", "1 D"))
        std::cout << "OK" << endl;
        

    std::cout << "\n--- Testing split-/joinValue()..." << endl;
        
    std::cout << "    - split..." << endl;
    std::vector<std::string> temp1 = init_obj_3.splitValue(init_obj_3.getValue("S3", "3 B"));
        
    for (std::vector<std::string>::iterator i = temp1.begin(); i != temp1.end(); ++i)
    {
            std::cout << *i << endl;
    }
    
    std::cout << "    - join..." << endl;      
    std::string tstr1 = init_obj_3.joinValue(temp1);
    std::cout << tstr1 << endl;
    
    std::cout << "\n--- Writing to file..." << endl;   
    
    init_obj_1->writeFile("out1.ini");
    init_obj_2->writeFile("out2.ini");
    init_obj_3.writeFile("out3.ini");
    init_obj_4.writeFile("out4_UTF-8.ini");
    
    std::cout << "\n--- Deleting objects..." << endl;
    
    delete init_obj_1;
    delete init_obj_2;
    
    std::cout << "\n--- Done with test!" << endl;

    return 0;
}
