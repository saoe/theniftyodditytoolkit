// Test/Demo/Example code for 'TextUtilities'.

#include "TextUtilities.h"
#include <iostream>


namespace txt = NiftyOddity::Toolkit::TextUtilities;

int main (int argc, char* argv[])
{
    std::cout << "\n\n===================================================================\n";
    std::cout << "Testing replacePlaceholder()\n";
    
    std::string strA = "Variable 1: $var1$\nVariable 2: $var2$\nVariable 3: $var3$\nVariable X: $var2$ (same as Variable 2)\n";
    std::cout << "\n--- String A: Input:\n" << strA;
    std::pair<std::string,std::string> Aa("$var1$", "AAA");
    std::pair<std::string,std::string> Ab("$var2$", "BBB");
    std::pair<std::string,std::string> Ac("$var3$", "CCC");
    std::cout << "\n--- String A: Output:\n" << txt::replacePlaceholder(strA, Aa, Ab, Ac);
    
    std::string strB = "Variable 1: [var1]\nVariable 2: [var2]\nVariable 3: [var3]\nVariable X: [var2] (same as Variable 2)\n";
    std::cout << "\n--- String B: Input:\n" << strB;
    std::pair<std::string,std::string> Ba("[var1]", "AAA");
    std::pair<std::string,std::string> Bb("[var2]", "BBB");
    std::pair<std::string,std::string> Bc("[var3]", "CCC");
    std::cout << "\n--- String B: Output:\n" << txt::replacePlaceholder(strB, Ba, Bb, Bc);
    
    std::string strC = "Variable 1: @var1@\nVariable 2: @var2@\nVariable 3: @var3@\nVariable X: @var2@ (same as Variable 2)\n";
    std::cout << "\n--- String C: Input:\n" << strC;
    std::pair<std::string,std::string> Ca("@var1@", "AAA");
    std::pair<std::string,std::string> Cb("@var2@", "BBB");
    std::pair<std::string,std::string> Cc("@var3@", "CCC");
    std::cout << "\n--- String C: Output:\n" << txt::replacePlaceholder(strC, Ca, Cb, Cc);
    
    std::string strD = "Variable 1: {var1}\nVariable 2: {var2}\nVariable 3: {var3}\nVariable X: {var2} (same as Variable 2)\n";
    std::cout << "\n--- String D: Input:\n" << strD;
    std::pair<std::string,std::string> Da("{var1}", "AAA");
    std::pair<std::string,std::string> Db("{var2}", "BBB");
    std::pair<std::string,std::string> Dc("{var3}", "CCC");
    std::cout << "\n--- String D: Output:\n" << txt::replacePlaceholder(strD, Da, Db, Dc);
    
    std::string strE = "Variable 1: {var1}\nVariable 2: [var2]\nVariable 3: @var3@\nVariable X: [var2] (same as Variable 2)\n";
    std::cout << "\n--- String E: Input:\n" << strE;
    std::pair<std::string,std::string> Ea("{var1}", "AAA");
    std::pair<std::string,std::string> Eb("[var2]", "BBB");
    std::pair<std::string,std::string> Ec("@var3@", "CCC");
    std::cout << "\n--- String E: Output:\n" << txt::replacePlaceholder(strE, Ea, Eb, Ec);
    
    std::cout << "\n\n===================================================================\n";
    std::cout << "Testing splitString()\n\n";
    
    std::string splitString_input = "value-1;\"string with space\";more;values_2;another string with space (but without quotes)";
    char const delimiter = ';';
    
    std::cout << "-> Input: " << splitString_input << "\n" << std::endl;
    
    std::vector<std::string> splitString_output1 = txt::splitString(splitString_input); // Space (' ') by default.
    std::vector<std::string> splitString_output2 = txt::splitString(splitString_input, delimiter);
    
    std::cout << "-- Split on space (default) --\n" << std::endl;
    for (typename std::vector<std::string>::iterator pos = splitString_output1.begin(); pos != splitString_output1.end(); ++pos)
    {
        std::cout << *pos << std::endl;
    }
    
    std::cout << "\n-- Split on delimiter (" << delimiter << ") --\n" <<std::endl;
    for (typename std::vector<std::string>::iterator pos = splitString_output2.begin(); pos != splitString_output2.end(); ++pos)
    {
        std::cout << *pos << std::endl;
    }
    
    std::cout << "\n\n===================================================================\n";
    std::cout << "Testing joinStrings()";
    
    std::cout << "\n\n--- Testing joinStrings(): Input: std::vector<std::string>\n";
    std::string joinString_output = txt::joinStrings(',', splitString_output1);
    std::cout << joinString_output;
    
    std::cout << "\n\n--- Testing joinStrings(): Input: Multiple strings\n";
    std::string joinString_x = txt::joinStrings(';', "abc", "def", "ghi");
    std::cout << joinString_x << std::endl;
    
    return 0;
}