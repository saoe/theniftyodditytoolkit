// Test/Demo/Example code for 'Log', the logging utility class.

#include "Log.hpp"
#include <iostream>

namespace tnot = NiftyOddity::Toolkit;
using tnot::Log;

int main (int argc, char* argv[])
{
    std::cout << "==== Testing Log ====\n";

    try
    {
        // Note: Using block scopes, so that the destructors get invoked early and print the "Log saved to..."
        // message after each block scope, instead all of them at once when the program ends.
        {
            
            std::cout << "\n-- L1: to file (path), UTC --\n";
            Log l1("C:\\temp\\log.txt", Log::OpenMode::Append, Log::TimeZone::UTC);
            l1.log("A trace text (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Trace, "Category 1");
            l1.log("A debug text (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Debug, "Category 2");
            l1.log("An info text (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Info /*, No category */);
            l1.log("A warning text (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Warning, "Category 4");
            l1.log("An error text (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Error, "Category 5");
            l1.log("A fatal error (UTC); UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Fatal, "Category 6");
        }

        {
            std::cout << "\n-- L2: To clog stream, local time --\n";
            Log l2(Log::Stream::clog);
            l2.log("A trace text (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Trace, "Category A");
            l2.log("A debug text (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Debug /*, No category */);
            l2.log("An info text (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Info, "Category C");
            l2.log("A warning text (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Warning /*, No category */);
            l2.log("An error text (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Error /*, No category */);
            l2.log("A fatal error (local time) to clog; UTF-8: ąčęėįšųūž + ©", Log::SeverityLevel::Fatal /*, No category */);
        }

        {
            std::cout << "\n-- L3: Convenience Functions; to cout stream and file [truncate, in working directory], local time --\n";
            Log l3(Log::Stream::cout, "", Log::OpenMode::Truncate);
            l3.trace("trace");
            l3.debug("debug");
            l3.info("info");
            l3.warning("warning", "A category");
            l3.error("error");
            l3.fatal("fatal");

            std::cout << "\n-- L3: Save the previous log additionally as a seperate CSV file [truncate, in working directory] --\n";
            l3.save("foo.csv", Log::OpenMode::Append, Log::ExportFileFormat::CSV);
        }

        {
            std::cout << "\n-- L4: Super simple constructor for stream output, using only default values --\n";
            Log l4; // Don't use "()", else it would be interpreted as a function declaration (-> "Most Vexing Parse").
            l4.info("Printing something to std::cout");
        }

        {
            std::cout << "\n-- L5: Super simple constructor for file output --\n";
            Log l5("");
            l5.info("Writing something to the default file");
        }

        {
            std::cout << "\n-- L6: Filter: Printing all 6 types of messages, but only Warning or higher should be considered --\n";
            Log l6(Log::Stream::cout, Log::TimeZone::Local, Log::SeverityLevelFilter::WarningOrHigher);
            l6.trace("L6 trace");
            l6.debug("L6 debug");
            l6.info("L6 info");
            l6.warning("L6 warning");
            l6.error("L6 error");
            l6.fatal("L6 fatal");
        }

    }
    catch (const std::invalid_argument& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    return 0;
}