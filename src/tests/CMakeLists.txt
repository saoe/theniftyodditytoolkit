# 
# Tests
# 
# Copyright © 2022 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT-0
# Part of "The Nifty Oddity Toolkit (TNOT)"
#

project ("Tests")

# --------------------------------------------------------------------------------------------------
# The targets for the test programs.
#
# Using the explicit version "add_test (NAME x COMMAND y)", because "add_test (x y)" seems to behave
# slightly different, according to the CMake documentation(?):
# > "Unlike the above NAME signature no transformation is performed on the command-line to support target names or generator expressions."

set (WinResFile ${CMAKE_SOURCE_DIR}/resources/windows-resources.rc)

add_executable (CastConvert-Test CastConvert-Test.cpp ${WinResFile})
target_link_libraries (CastConvert-Test CastConvert)
add_test (NAME CastConvert-Test COMMAND CastConvert-Test)

add_executable (CLI-Console-Test CLI-Console-Test.cpp ${WinResFile})
target_link_libraries (CLI-Console-Test CLI_Console)
add_test (NAME CLI-Console-Test COMMAND CLI-Console-Test)

add_executable (CLI-Options-Test CLI-Options-Test.cpp ${WinResFile})
target_link_libraries (CLI-Options-Test CLI_Options)
add_test (NAME CLI-Options-Test COMMAND CLI-Options-Test)

add_executable (CLI-Utilities-Test CLI-Utilities-Test.cpp ${WinResFile})
target_link_libraries (CLI-Utilities-Test CLI_Utilities)
add_test (NAME CLI-Utilities-Test COMMAND CLI-Utilities-Test)

add_executable (InitFile-Test InitFile-Test.cpp ${WinResFile})
target_link_libraries (InitFile-Test InitFile)
add_test (NAME InitFile-Test COMMAND InitFile-Test)

add_executable (KeyboardLED-Test KeyboardLED-Test.cpp ${WinResFile})
target_link_libraries (KeyboardLED-Test KeyboardLED)
add_test (NAME KeyboardLED-Test COMMAND KeyboardLED-Test)

add_executable (Log-Test Log-Test.cpp ${WinResFile})
target_link_libraries (Log-Test Log)
add_test (NAME Log-Test COMMAND Log-Test)

add_executable (Stopwatch-Test Stopwatch-Test.cpp ${WinResFile})
target_link_libraries (Stopwatch-Test Stopwatch)
add_test (NAME Stopwatch-Test COMMAND Stopwatch-Test)

add_executable (TextUtilities-Test TextUtilities-Test.cpp ${WinResFile})
target_link_libraries (TextUtilities-Test TextUtilities)
add_test (NAME TextUtilities-Test COMMAND TextUtilities-Test)

add_executable (Win32GUI-Test Win32GUI-Test.cpp ${CMAKE_SOURCE_DIR}/resources/windows-resources.rc)
target_link_libraries (Win32GUI-Test Win32GUI)
add_test (NAME Win32GUI-Test COMMAND Win32GUI-Test)

add_executable (FileUtilities-Test FileUtilities-Test.cpp ${WinResFile})
target_link_libraries (FileUtilities-Test FileUtilities)
add_test (NAME FileUtilities-Test COMMAND FileUtilities-Test)