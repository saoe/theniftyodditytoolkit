//
// Test/Demo/Example for code in the 'CLI' section.
//

#include <iostream>
#include <thread>
#include <chrono>
#include "Console.hpp"   // For a test file inside the source folder, these are OK.

namespace con  = NiftyOddity::Toolkit::CLI::Console;

int main (int argc, char* argv[])
{
    // ---------------------------------------------------------------------------------------------

    std::cout << "\n________ Starting with a few ANSI escape code gimmicks ________" << std::endl;
    
    // This class' constructor and destructor prepare/restore the console for ANSI.
    // [TODO] Just needed for initialization, the functions are static -- is that a good design or required?
    con::Console CC;
	
	std::cout << con::Console::Status::Error   << "This is an error text. "          << con::Console::Status::Reset << "Default text." << std::endl;
	std::cout << con::Console::Status::Warning << "This text is a warning message. " << con::Console::Status::Reset << "Default text." << std::endl;
	std::cout << con::Console::Status::Info    << "This is an INFO text. "           << con::Console::Status::Reset << "Default text." << std::endl;
	std::cout << con::Console::Status::OK      << "This text is an OK message. "     << con::Console::Status::Reset << "Default text." << std::endl;
    
    std::cout << '\n';
    
    std::cout << con::Console::TextColor::Green << "Default background and green text" << '\n'
              << con::Console::TextColor::BrightGreen << "Default background and bright green text" << '\n'
              << std::endl;
    
    std::cout << con::Console::BackgroundColor::Yellow << "Text (with that last set color!) on yellow background."
              << con::Console::BackgroundColor::ResetAll /* could also have used TextColor::... */ << '\n'
              << "All Colors were reset." << '\n'
              << con::Console::BackgroundColor::BrightBlue << con::Console::TextColor::BrightYellow << "Bright yellow text on blue background."
              << con::Console::BackgroundColor::Reset << '\n' << "Background Color was reset to the default." << '\n'
              << con::Console::TextColor::Reset << "Text Color was also reset." << std::endl;
    
    std::cout << "\n+++ Styles (some may not be supported by this terminal) +++" << std::endl;
    std::cout << con::Console::Style::Bold << "Bold (probably rendered as bright text...)" << con::Console::Style::Reset << "\n"
              << con::Console::Style::Faint << "Faint" << con::Console::Style::Reset << "\n"
              << con::Console::Style::Invert << "Inverted background/foreground color" << con::Console::Style::Reset << "\n"
              << con::Console::Style::Underline << "Underline" << con::Console::Style::Reset << "\n"
              << std::endl;
    
    std::cout << "________ Cursor navigation ________" << std::endl;
    
    std::cout << con::Console::TextColor::BrightYellow 
    << "0123456789a123456789b123456789c123456789d123456789e123456789f123456789g123456789h\n"
    << "01 ------------------------------------------------------------------------------\n"
    << "02 ------------------------------------------------------------------------------\n"
    << "03 ------------------------------------------------------------------------------\n"
    << "04 ------------------------------------------------------------------------------\n"
    << "05 ------------------------------------------------------------------------------\n"
    << "06 ------------------------------------------------------------------------------\n"
    << "07 ------------------------------------------------------------------------------\n"
    << "08 ------------------------------------------------------------------------------\n"
    << "09 ------------------------------------------------------------------------------\n"
    << "10 ------------------------------------------------------------------------------\n"
    << con::Console::TextColor::Reset;
    
    std::cout << con::Console::moveCursor(con::Console::CursorNavigation::Up, 5, false)
    << con::Console::moveCursor(con::Console::CursorNavigation::Right, 10, false)
    << "Cursor was moved 5 cells up & 10 cells right (embedded function).\n";
    
    // Back to normal, for the next tests.
    std::cout << con::Console::moveCursor(con::Console::CursorNavigation::Down, 5, false)
    << con::Console::moveCursor(con::Console::CursorNavigation::Left, 10, false)
    << "Cursor moved back again.\n";
    
    std::cout << "\n________ Clear Line/Clear Screen ________" << std::endl;
      
    std::cout << "Normal text again to std::cout -- wait for it..." << std::endl;
    
    std::this_thread::sleep_for(std::chrono::seconds(3)); // So that the tester can notice what happens next...
    con::Console::moveCursor(con::Console::CursorNavigation::LineUp);
    std::cout << "Normal text ~ PARTIALLY OVERWRITTEN ~" << std::endl;
    
    //std::cout << "________ Clear ________" << std::endl;
    //
    //con::Console::moveCursorToPosition(5, 10);
    //std::cout << "* TEST *";
    //std::cout << "* TEST 2 *";
    
    // ??? con::Console::clear(con::Console::Clear::Screen);
    //con::Console::clear(con::Console::Clear::FromCursorToEndOfScreen);
    //con::Console::clear(con::Console::Clear::FromCursorToStartOfScreen);
    //con::Console::clear(con::Console::Clear::EntireScreen);
    //con::Console::clear(con::Console::Clear::Line);
    //con::Console::clear(con::Console::Clear::FromCursorToEndOfLine);
    //con::Console::clear(con::Console::Clear::FromCursorToStartOfLine);
    //con::Console::clear(con::Console::Clear::EntireLine);
  
    return 0;
}
