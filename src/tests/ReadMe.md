# Tests

## Build

The test executable are a target like any other of the project and should then be run them manually.

You can build them all at once (together with all the other targets):

    cmake --build --preset <name_of_a_BUILD_preset>
    
... or individually:

    cmake --build --preset <name_of_a_BUILD_preset> --target <name_of_a_test>

## A note on using CTest

You could also use _CTest_, but **please note that at the moment, the tests are not designed for such use case!**

Running with CTest is a kind of unattended, continuous-integration run; and my tests (demos, examples, ...)
currently aren't made for it: They don't return the right feedback to get a _pass/fail_ state from it;
and to understand whether they work correctly or not, they must be watched and interpreted by an human.

This may (or may not) change in the future, but that's the status now (2022-03-02).

Nevertheless, the basic foundation has already been laid; so here are some initial remarks:

1. CTest must be invoked from the build tree:

    C:\TheNiftyOddityToolkit\> cd build

2. For a generator that provides multiple build configurations in the same tree (like MSVC does),
   a configuration (`-C`, `--build-config`) argument is also required, e.g. "Release" or "Debug":

    C:\TheNiftyOddityToolkit\build\> ctest -C Release

3. One can also filter the test names (via RegEx, `-R`), so that not everything will be run:

    C:\TheNiftyOddityToolkit\build\> ctest -C Release -R PartialNameOfATestTarget
    
