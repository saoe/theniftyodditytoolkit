/*
    Simple demo program that handles command line options.
    Copyright © 2016-2018, 2021-2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
*/

#include <iostream>
#include "Options.hpp"

namespace opt = NiftyOddity::Toolkit::CLI::Options;

int main (int argc, char* argv[])
{   
    // Iterate over the command line arguments; ignore the first argument (that's always the program name).
    for (int i = 1; i < argc; i++)
    {
        std::string argument;
        
        if (opt::isHelpSwitch(argc, argv[i]))
        {
            std::cout << "Help text for program " << argv[0] << '\n';
            
            std::cout << R"(
General Help
------------

* Case sensitivity is enabled/disabled for each function call individually by the developer.
* Argument order is irrelevant.
* Multiple occurrences of the same option are fine (all appearances will be noted;
  it's up to the application code to handle such cases.)
* One can use either "/" or "-" (or mix both styles) as a prefix character for an option.
* One can use either "=" or ":" (or mix both styles) as a delimiter between key and value.
* Spaces must be enclosed in quotation marks ("text with spaces"), to be handled correctly;
  nesting those is not possible.
* The combination (or folding) of one-character arguments is currently _not_ supported:
  -abc is just one argument, not three single-character arguments combined
* The seperation of short and long form options is currently not supported:
  '-w' is not a shortform for something like '--wordy-argument'.


These are the possible values for this test/example program
-----------------------------------------------------------

h|help|?: Print this help.
    
    Examples:
        -h
        /help
        -?

o1: Simple switch option:
    
    Examples:
        /o1
        -o1

o2: Key option with an assigned value:
    
    Examples:
        o2=SomeValue
        o2:SomeValue
        o2="Some value"

    Note: There is no prefix ('-' or '/') character at the beginning!

o3: Switch option with an assigned value:
    
    Examples:
        /o3=foo
        /o3:"blah blah"

o4 (+ FixKey): Switch with a predefined key ('FixKey' here) and its value:
    
    Examples:
        /o4 FixKey:SomeValue
        /o4 FixKey="Some value"

o5: A switch with an associated value:
    
    Examples:
        /o5 "Some value"
        -o5 1234
)" << std::endl;
            return 0;
        }
        else if (opt::isSwitch(argv[i], "o1"))
        {
            printf("Option o1 detected: a switch option\n");
        }
        else if (opt::isKeyValue(argv[i], "o2", &argument))
        {
            printf("Option o2 detected (a key with an assigned value of '%s')\n", argument.c_str());
        }
        else if (opt::isSwitchWithValue(argv[i], "o3", &argument))
        {
            printf("Option o3 detected (a switch with an assigned value of '%s')\n", argument.c_str());
        }
        else if (opt::isSwitchWithKeyValue(argv, i, "o4", "FixKey", &argument))
        {
            printf("Option o4 detected (a switch followed by a key 'FixKey' and value '%s')\n", argument.c_str());
        }
        else if (opt::isSwitchAndValue(argv, i, "o5", &argument))
        {
            printf("Option o5 detected (a switch with an associated value of '%s')\n", argument.c_str());
        }
        else
        {
            printf("Unknown option '%s'\nTry '-h' for help.\n", argv[i]);
            return 0;
        }
    }
    
    return 0;
}
