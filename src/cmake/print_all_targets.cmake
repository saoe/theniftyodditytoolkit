#
#                        Collect all targets in all subdirectories of a project
#
# Based on https://stackoverflow.com/a/60232044 (from 2020-02-14, found 2021-02-22)
# Call it at the end of the top-level CMakeLists.txt
# Will only be executed during the configuration step!
#

function (get_all_targets ResultList StartDirectory)
    # Parameters:
    # - ResultList    : The list variable that should contain all found targets.
    # - StartDirectory: The directory from where we begin to start looking.

    get_property (_subdirs DIRECTORY "${StartDirectory}" PROPERTY SUBDIRECTORIES)
    foreach (_subdir IN LISTS _subdirs)
        get_all_targets(${ResultList} "${_subdir}")
    endforeach ()

    get_directory_property (_sub_targets DIRECTORY "${StartDirectory}" BUILDSYSTEM_TARGETS)
    set (${ResultList} ${${ResultList}} ${_sub_targets} PARENT_SCOPE)
endfunction ()

function (print_all_targets)
    get_all_targets(all_targets ${CMAKE_CURRENT_LIST_DIR})
    
    message(STATUS "[${PROJECT_NAME}] Custom targets defined in this project (the CMake standard targets are excluded):")
    #list (FILTER all_targets EXCLUDE REGEX "_Internal$") # "*_Internal" are custom/internal targets, used as workarounds for the implementation; see corresponding CMakeLists.txt for details.
    
    foreach (item IN LISTS all_targets)
        message(STATUS "* ${item}")
    endforeach ()
endfunction ()

# Deferring Calls: Available since CMake version 3.19.
cmake_language(DEFER CALL print_all_targets)
