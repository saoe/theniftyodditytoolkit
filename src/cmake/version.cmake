#
# Script to get the timestamp (BUILDTIME) and the Git commit hash of 'head' (GIT_COMMIT_HASH).
# 
# Usage in a CMakeLists.txt file:
#
# add_custom_target(VersionInfo 
#     ${CMAKE_COMMAND}                               # CMake unfortunately doesn't make it easy to hand over/access variables, so we have to define them here for the *.cmake file.
#       -D IN=${CMAKE_CURRENT_LIST_DIR}/version.h.in # Input file, that contains placeholders like @BUILDTIME@ and @GIT_COMMIT_HASH@.
#       -D OUT=${CMAKE_CURRENT_BINARY_DIR}/version.h # The generated output file, where the placeholders are replaced with the real values. Put in the build directory!
#       -D SOMETHING=${SOMETHING}
#       -P ${CMAKE_SOURCE_DIR}/cmake/version.cmake   # This script.
# )
#
# Copyright © 2015, 2019, 2021-2022 Sascha Offe <so@saoe.net>.
# SPDX-License-Identifier: MIT-0

# Timestamp of the build (local time; append the optional parameter 'UTC' for Coordinated Universal Time).
string (TIMESTAMP BUILDTIME "%Y-%m-%d %H:%M:%S")

find_package(Git)

# Note that the following expects a specific directory structure; so adapt to your own style if required:
#
# Project/
# +-- .git/
# +-- src/
#     +--  CMakeLists.txt   <-- top-level CMake file
#
get_filename_component (ONE_DIRECTORY_ABOVE_TOPLEVELDIR ${TOPLEVELDIR}/.. ABSOLUTE)

# Get full length SHA-1 Git commit hash of the current 'HEAD' revision (the unique identifier of the pointer to the current local branch).
#
if (GIT_FOUND AND EXISTS "${ONE_DIRECTORY_ABOVE_TOPLEVELDIR}/.git")
	execute_process (
		 COMMAND         ${GIT_EXECUTABLE} rev-parse HEAD
		 OUTPUT_VARIABLE GIT_COMMIT_HASH
		 OUTPUT_STRIP_TRAILING_WHITESPACE
	)
endif ()

# Load the license text into a variable and change it a bit before handing it over:
# configure_file seems to convert it from UTF-8 back to ANSI, or maybe the Windows Resource Compiler
# has problems due to other reasons: "Error RC2018: unknown character '0x40'" -> That's the At-Sign (@).
file (READ "${ONE_DIRECTORY_ABOVE_TOPLEVELDIR}/LICENSE.txt" LICENSE_TEXT)
string (REPLACE "©" "(c)" LICENSE_TEXT ${LICENSE_TEXT})
string (REPLACE "@" "(at)" LICENSE_TEXT ${LICENSE_TEXT})

# Substitute the placeholders with real values and create an usable header file.
configure_file (${IN} ${OUT} @ONLY)
