/*!
    @file
    Utility functions for casting and converting data types.
    
    Copyright © 2006, 2016, 2018-2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)
    
    @namespace NiftyOddity::Toolkit::CastConvert
    @copybrief CastConvert.h

    @details
    - Provided as a 'header-only library' (thus the inline specifiers).
    - Only tested under Microsoft Windows.
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_046AC2B4_26CF_4DA3_B5B3_88A2C3603B1C
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_046AC2B4_26CF_4DA3_B5B3_88A2C3603B1C

#include <iomanip> 
#include <string>
#include <sstream>
#include <vector>
#include <windows.h>

namespace NiftyOddity::Toolkit::CastConvert
{
/*!
    Translates a given number into a text string.
    
    @tparam T Integer type
    
    @param number The number/integer that should be translated into a text string.
    
    @returns Standard text string
*/

template <typename T>
std::string convertNumberToText (T number)
{
	std::ostringstream oss;
	oss << number;
	return oss.str();
}


/*!
    Translates a given amount of bytes into an easier readable text string (with unit suffix).
    
    Casts a number that represents a big number of bytes into a std::string (including descriptive unit text) and returns it.  
    Automatically calculates the best fitting size for the amount of bytes.
    
    Example: The input is `1048576` (bytes), the function will return the string _1.00 Megabytes_.
    
    @tparam T Integer type
    
    @param bytes_count The amount of bytes
    
    @returns Standard text string
*/

template <typename T>
std::string translateBytesToHandyText (T bytes_input)
{	
	std::vector<std::string> storage_unit = {"Bytes",
	                                         "Kilobytes",
	                                         "Megabytes",
	                                         "Gigabytes",
	                                         "Terabytes",
	                                         "Petabytes",
	                                         "Exabytes"  };

	int   i = 0;
	float t = bytes_input;

	while (t >= 1024 && i < storage_unit.size())
	{
		t /= 1024;
		i++;
	}

	std::stringstream ss;
	ss << std::fixed << std::setprecision(2) << t;
	return ss.str() + " " + storage_unit.at(i);
}


/*!
    Translates a given amount of bytes into an easier readable text string (with unit suffix).
    
    Converts a number that represents a big number of bytes into text (as char*), including descriptive unit text.  
    Automatically calculates the best fitting size for the amount of bytes.
    
    Example: The input is `1048576` (bytes), the function will return the string _1.00 Megabytes_.
    
    @tparam T Integer type
    
    @param [in]  bytes_input The amount of bytes
    @param [out] destination The destination buffer
    @param [in]  n           The size of the destination buffer
*/

template <typename T>
void translateBytesToHandyText (T bytes_input, char* destination, size_t n)
{
    std::string s = translateBytesToHandySizeText(bytes_input);
	strncpy(destination, s.c_str(), n-1);
	destination[n-1] = '\0';
}


/*!
    Translates a given amount of seconds into an easier readable text string (with unit suffixes).
    
    Casts a number that represents a big number of seconds into a std::string and returns it.  
    Automatically calculates the best fitting sizes for the amount of seconds.
    
    Example: The input is `90061` (seconds), the function will return the string _1d:01h:01m:01s_.
    
    Explanation:
    - Second (s): The SI (International System of Units) base unit for time.
    - Minute (m): 60 seconds; the SI unit symbol is "min" ("m" is for metre), but I use "m" here, because it's shorter.
    - Hour (h): 60 minutes.
    - Day (d): 24 hours (although there may be variations due daylight saving time and leap seconds, but that should not matter here).
    - Week (w): 7 days; couldn't find any different commonly used length in modern times.
    - Month: Omitted, since it's not fix: Varies between 28-31 days.
    - Year: Omitted, since it's not fix: Varies between 365-366 days (leap year).
    
    @tparam T Integer type
    
    @param seconds The amount of seconds to translate
    
    @return Standard text string
*/

template <typename T>
std::string translateSecondsToHandyText (T seconds)
{
    // ('seconds' is given as the input)
    unsigned int remaining_seconds = seconds % 60;

    unsigned int minutes           = (seconds - remaining_seconds) / 60;
    unsigned int remaining_minutes = minutes % 60;

    unsigned int hours           = (minutes - remaining_minutes) / 60;
    unsigned int remaining_hours = hours % 24;

    unsigned int days = (hours - remaining_hours) / 24;
    unsigned int remaining_days = days % 7;

    unsigned int weeks = (days - remaining_days) / 7;

    // -- For troubleshooting: --
    // std::cout << "--------------------------------------\n";
    // std::cout << "remaining_seconds: " << remaining_seconds << std::endl;
    // std::cout << "remaining_minutes: " << remaining_minutes << std::endl;
    // std::cout << "remaining_hours  : " << remaining_hours << std::endl;
    // std::cout << std::endl;
    // std::cout << "seconds (input)  : " << seconds << std::endl;
    // std::cout << "minutes          : " << minutes << std::endl;
    // std::cout << "hours            : " << hours << std::endl;
    // std::cout << "days             : " << days << std::endl;
    // std::cout << '\n';

    std::stringstream output_string;

    // No leading zeros on the output if the first unit of the line has only one digit.
    int h_width = hours < 10 ? 1 : 2;
    int m_width = minutes < 10 ? 1 : 2;
    int s_width = seconds < 10 ? 1 : 2;

    // Compose the output string.
    if (weeks > 0) { output_string << weeks << "w:"; }

    if (remaining_days >= 0 && weeks > 0) { output_string << std::setw(1) << remaining_days << "d:"; }
    else if (days > 0)                    { output_string << days << "d:"; }

    if (remaining_hours >= 0 && days > 0) { output_string << std::setfill('0') << std::setw(2) << remaining_hours << "h:"; }
    else if (hours > 0)                   { output_string << std::setfill('0') << std::setw(h_width) << hours << "h:"; }
    
    if      (remaining_minutes >= 0 && hours > 0) { output_string << std::setfill('0') << std::setw(2) << remaining_minutes << "m:"; }
    else if (minutes > 0)                         { output_string << std::setfill('0') << std::setw(m_width) << minutes << "m:"; }
    
    output_string << std::setfill('0') << std::setw(s_width) << remaining_seconds << "s";
    
    return output_string.str();
}


/*!
    Narrows a wide string (conversion from UTF-16 to UTF-8).
    
    @param wstr A wide string that should be narrowed.
    
    @returns A narrow string (standard string type).
    
    @note
    - http://utf8everywhere.org/
    - http://www.nubaria.com/en/blog/?p=289
    
    @todo
    If the compiler supports the "C++11" standard:
    This can be implemented in a portable way, by defining a variable of type `std::wstring_convert <std::codecvt_utf8_utf16 <Utf16Char>, Utf16Char>`
    and calling `to_bytes()` and `from_bytes()` for the UTF-8 to UTF-16 and UTF-16 to UTF-8 conversions, respectively.
    Visual Studio 2012 supposedly supports this. -- <http://www.nubaria.com/en/blog/?p=289>
*/

inline
std::string narrowString (const std::wstring& wstr)
{
    std::string str;
	int required_size = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if(required_size > 0)
	{
		std::vector<char> buffer(required_size);
		WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, &buffer[0], required_size, 0, 0);
		str.assign(buffer.begin(), buffer.end() - 1);
	}
	return str;
}


/*!
    Widens a narrow string (conversion from UTF-8 to UTF-16).
    
    @param str A narrow string that should be widened.
    
    @returns A reference(?) to a wide standard string type.
        
    @note
    - http://utf8everywhere.org/
    - http://www.nubaria.com/en/blog/?p=289
        
    @todo
    If the compiler supports the "C++11" standard:
    This can be implemented in a portable way, by defining a variable of type 'std::wstring_convert <std::codecvt_utf8_utf16 <Utf16Char>, Utf16Char>'
    and calling to_bytes() and from_bytes() for the UTF-8 to UTF-16 and UTF-16 to UTF-8 conversions, respectively.
    Visual Studio 2012 supposedly supports this. -- <http://www.nubaria.com/en/blog/?p=289>
*/
    
inline
std::wstring widenString (const std::string& str)
{
	std::wstring wstr;
	int required_size = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
	if(required_size > 0)
	{
		std::vector<wchar_t> buffer(required_size);
		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], required_size);
		wstr.assign(buffer.begin(), buffer.end() - 1);
	}
 	return wstr;
}

} // namespace

#endif // include guard