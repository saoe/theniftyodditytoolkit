/*!
    @file
    Implementation of class 'Log'.
    
    Copyright © 2022 Sascha Offe <so@saoe.net>  
    SPDX-License-Identifier: MIT-0  
    For details, see file LICENSE.txt  
    Part of [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/)

    @namespace NiftyOddity::Toolkit::Log
    @copybrief Log.hpp
    
    @internal
    2016-04-07 (so): Started in repository 'misc_public' as c_cpp\log.h.
    2022-03-01 (so): Moved to TNOT and began refactoring and extending it.
    2022-03-11 (so): So far, so good (for now); time to put it in use in my projects.
    
    @todo One feature that _may_ be interesting for the future: Filtering.
    Meaning some kind of filtering mechanism for the logged records (e.g. "only level x, and newer as
    time t, and only from category c, ..."). There are some ways in C++20 (ranges, range adapters, pipe
    operator, ...) that sound interesting for that, but I only looked very briefly at it. I need to
    learn more about it first (and how cumbersome that may be to implement, especially if you want it to
    be flexible).
    But I also don't think that I will ever log so much data with this class that other, more pragmatic
    ways wouldn't be easier; for example simply dumping all records via save() to a CSV file and then
    filtering them via a Powershell pipe -- so maybe it would be a wasted effort (except for the
    learning experience for the aforementioned C++ features; hmm...)
*/

#ifndef INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_6738720C_BD48_47C5_B849_F625C4863285
#define INCLUDE_GUARD_THENIFTYODDITYTOOLKIT_6738720C_BD48_47C5_B849_F625C4863285
 
#include <chrono>
//#include <codecvt>         // For UTF-8 support.
#include <filesystem>
#include <fstream>
//#include <format>          // Use std::format instead of printf or plain iostream. [C++20]
    // Not ready (yet?) in MSVC 2019 16.11 with /std:c++20:
    // "The contents of <format> are available only in c++latest mode with concepts support;
    // see https://github.com/microsoft/STL/issues/1814 for details."
#include <iostream>
//#include <locale>          // For UTF-8 support.
#include <map>
#include <optional>
#include <source_location> // Use std::source_location instead of __FILE__ etc. [C++20]
#include <sstream>         // Use stringstream until C++20's "format" is fully supported by MSVC.
#include <string>
#include <string_view>
#include <vector>

namespace NiftyOddity::Toolkit
{

/*!
    A simple header-only utility class to help logging data to text files and/or output streams.
*/
class Log
{
    public:
        enum class TimeZone
        {
            UTC,
            Local
        };
        
        enum class OpenMode
        {
            Append,
            Truncate
        };

        enum class ExportFileFormat
        {
            CSV
        };

        enum class SeverityLevel : int
        {
            Trace   = 1,
            Debug   = 2,
            Info    = 3,
            Warning = 4,
            Error   = 5,
            Fatal   = 6 
        };

        enum class SeverityLevelFilter : int
        {
            All             = 0,
            TraceOrHigher   = 1,
            DebugOrHigher   = 2,
            InfoOrHigher    = 3,
            WarningOrHigher = 4,
            ErrorOrHigher   = 5,
            OnlyFatal       = 6 
        };


        enum class Stream
        {
            cout, //!< to standard output (std::cout).
            cerr, //!< to standard error (std::cerr; limited buffering).
            clog  //!< to standard log (std::clog; similar to std::cerr, but comes with full buffering).
        };
        

        /*!
            Construct a Log object that can write to an output stream and/or a logfile (possibly simultaneously).

            @param s   The stream to which the log message should be sent.
                       If the argument value is `std::nullopt`, this will be ignored.
            @param fp  The path/name of the logfile.
                       If the argument value is an empty string (`""`), a default name will be used and the file will be created in the current working directory.
                       If the argument value is `std::nullopt`, this will be ignored.
            @param om  Controls whether the entries will be appended to an existing file, or whether the whole file will be overwritten (truncated).
            @param tz  Controls whether the timestamps should be in local time or in Coordinated Universal Time (UTC).
            @param slf Controls the threshold of which log message types will go into the records/will be printed.
        */
        Log (std::optional<Stream> s, std::optional<std::filesystem::path> fp, OpenMode om = OpenMode::Append, TimeZone tz = TimeZone::Local, SeverityLevelFilter slf = SeverityLevelFilter::All)
            : time_zone(tz)
            , output_stream(s)
            , output_filepath(fp)
            , os(std::cout.rdbuf()) // An ostream must be initialized.
            , filter(slf)
        {
            if (output_stream != std::nullopt)
            {
                if      (output_stream == Stream::cout) { os.rdbuf(std::cout.rdbuf()); }
                else if (output_stream == Stream::cerr) { os.rdbuf(std::cerr.rdbuf()); }
                else if (output_stream == Stream::clog) { os.rdbuf(std::clog.rdbuf()); }
            }

            if (output_filepath != std::nullopt)
            {
                if (output_filepath.value().string().empty())
                    output_filepath.emplace(std::filesystem::current_path()/("log_"+timestamp(tz, true)+".txt"));
                
                std::filesystem::path path(output_filepath.value());

                // Check if the path up to this point exists, because open() just silently fails if it does not.
                if (std::filesystem::exists(path.parent_path()))
                {
                    std::string file = path.string();
                    //utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
                        /*! @fixme Compiler Warning C4999: deprecated: std::codecvt_utf8<wchar_t>
                        I think this (the 'locale/imbue' stuff), and the fact to write as binary (see below)
                        was necessary to get a UTF-8 encodes text file (I adopted it from my 2016 Log.h version)
                        But currently it works also without (might be related to my current CMake/MSVC setup/configuration). */
                
                    switch (om)
                    {
                        case OpenMode::Append:
                            of.open(file, std::ios_base::app | std::ios::out | std::ios::binary);
                            break;
                
                        case OpenMode::Truncate:
                            of.open(file, std::ios_base::trunc | std::ios::out | std::ios::binary);
                            break;
                
                        default:
                            of.open(file, std::ios_base::app | std::ios::out | std::ios::binary);
                    }
                
                    //of.imbue(utf8_locale);
                }
                else
                {
                    throw std::invalid_argument("The provided path does not exist: " + path.parent_path().string());
                }
            }
        }


        /*!
            Construct a Log object that only writes to an output stream.

            @param s   The stream to which the log message should be sent.
                       By default, this is `cout`.
            @param tz  Controls whether the timestamps should be in local time or in Coordinated Universal Time (UTC).
                       By default, this is using the local time.
            @param slf Controls the threshold of which log message types will go into the records/will be printed.

            @internal
            Relays the data via a delegating constructor (constructor chaining) to the main constructor.
        */
        Log (Stream s = Stream::cout, TimeZone tz = TimeZone::Local, SeverityLevelFilter slf = SeverityLevelFilter::All)
            : Log(s, std::nullopt, OpenMode::Append, tz, slf)
        {
        }


        /*!
            Construct a Log object that only writes to a file.

            @param fp  The path/name of the logfile.
                       If you provide just an empty string (`""`) as the argument value, a default filename
                       will be used and the file will be created in the current working directory.
            @param om  Controls whether the entries will be appended to an existing file, or whether the whole file will be overwritten (truncated).
            @param tz  Controls whether the timestamps should be in local time or in Coordinated Universal Time (UTC).
                       By default, this is using the local time.
            @param slf Controls the threshold of which log message types will go into the records/will be printed.

            @internal
            - Relays the data via a delegating constructor (constructor chaining) to the main constructor.
            - Can't provide a default value for _fp_, because that would cause an an ambiguity with the other
              constructor and its default values.
        */
        Log (std::filesystem::path fp, OpenMode om = OpenMode::Append, TimeZone tz = TimeZone::Local, SeverityLevelFilter slf = SeverityLevelFilter::All)
            : Log(std::nullopt, fp, om, tz, slf)
        {
        }


        /*!
            Destructor
        */
        ~Log ()
        {
            if (of.is_open())
            {
                of.flush();
                of.close();
                os << "Log saved to " << output_filepath.value().string() << std::endl;
            }
        }
        

        /*!
            Write an entry to the log record and also output (if configured) to the stream and/or logfile.

            @param message  Free text for the log entry.
            @param l        The level of severity.
                            The log message will only be printed or recorded if the severity level specified
                            here is equal or higher than the severity level filter threshold of this Log instance (see Log::Log()).
            @param category Optional free text to categorize the entry better (e.g. by application, system, library, component, ...).
            @param location Should not be changed from the default value!
                            It is required to be a function argument, so that the correct calling function will be reported in the log.

            @internal
            Note (2022-03-07): The 'error' in the signature ("call to consteval function std::source_location::current()...") is just an IntelliSense bug;
            it compiles and works without an issue; should be fixed in newer versions <https://developercommunity.visualstudio.com/t/IntelliSense-error-with-std::source_loca/1424428>.
        */
        void log (std::string message, SeverityLevel l = SeverityLevel::Info, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            if (static_cast<int>(l) >= static_cast<int>(filter))
            {
                Entry e;
                e.timestamp = timepoint(time_zone);
                e.severity_level = l;
                e.catgegory = category != std::nullopt ? category.value() : "";
                e.message = message;
                e.path = std::filesystem::path(location.file_name());
                e.line = location.line();
                e.column = location.column();
                e.function = location.function_name();
                records.push_back(e);

                std::string origin, line;

                if (l == SeverityLevel::Info || l == SeverityLevel::Warning)
                    origin = e.path.filename().string() + " [Line " + std::to_string(location.line()) + "]";
                else
                    origin = e.path.string() + " [" + std::to_string(e.line) + ":" + std::to_string(e.column) + "] " + e.function;

                if (category == std::nullopt)
                    line = timestamp(time_zone) + " [" + SeverityLevelString[l] + "] " + message + " @ " + origin;
                else
                    line = timestamp(time_zone) + " [" + SeverityLevelString[l] + "][" + e.catgegory + "] " + message + " @ " + origin;
            
                if (output_stream != std::nullopt)   { os << line << std::endl; }
                if (output_filepath != std::nullopt) { of << line << std::endl; }
            }
        }

        //! A convenience function to log an entry with the same severity level that the function name implies.
        void trace (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Trace, category, location);
        }

        //! @copybrief trace()
        void debug (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Debug, category, location);
        }

        //! @copybrief trace()
        void info (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Info, category, location);
        }

        //! @copybrief trace()
        void warning (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Warning, category, location);
        }

        //! @copybrief trace()
        void error (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Error, category, location);
        }

        //! @copybrief trace()
        void fatal (std::string message, std::optional<std::string> category = std::nullopt, std::source_location location = std::source_location::current())
        {
            log(message, SeverityLevel::Fatal, category, location);
        }


        /*!
            Save the recorded log entries explicitly to a separate file in the specified export format.
         
            @param path Where to save the file; must be a path to a file or at least a filename, not just a directory.
                        If just a filename is specified, it will be saved in the current working directory.
            @param om   Controls whether the entries will be appended to an existing file, or whether the whole file will be overwritten (truncated).
            @param eff  In which format the file should be saved.

            @exception std::invalid_argument When a path doesn't exist, or a path is only a directory, or misses a filename, or
                                             if the export format is unknown.
        */
        void save (std::filesystem::path path, OpenMode om = OpenMode::Append, ExportFileFormat eff = ExportFileFormat::CSV)
        {
            if (!path.string().empty())
            {
                if (!std::filesystem::exists(path.parent_path()))
                    path = std::filesystem::current_path()/path;
            
                if (std::filesystem::exists(path.parent_path()))
                {
                    if (!std::filesystem::is_directory(path))
                    {
                        switch (eff)
                        {
                            case ExportFileFormat::CSV:
                                saveAsCSV(path, om);
                                break;

                            default:
                                throw std::invalid_argument("TNOT::Log::Save: ExportFileFormat is not valid"); // Why bother? Should already be catched at compilation time; oh well...
                        }
                    }
                    else
                    {
                        throw std::invalid_argument("TNOT::Log::Save: Path must not be a directory!");
                    }
                }
                else
                {
                    throw std::invalid_argument("TNOT::Log::Save: Path does not exist: " + path.parent_path().string());
                }
            }
            else
            {
                throw std::invalid_argument("TNOT::Log::Save: Filename is missing");
            }
        }

    private:
        TimeZone time_zone;
        SeverityLevel type;
        SeverityLevelFilter filter;
        
        std::optional<Stream> output_stream; // For checks which output stream (or if any at all!) should be used.
        std::ostream os;                     // The actual output stream.

        //std::locale utf8_locale;
        std::optional<std::filesystem::path> output_filepath; // For checks whether an output filepath was provided.
        std::ofstream of;                                     // The actual output file.

        std::map<SeverityLevel, std::string> SeverityLevelString
        {
            {SeverityLevel::Trace,   "Trace"},
            {SeverityLevel::Debug,   "Debug"},
            {SeverityLevel::Info,    "Info"},
            {SeverityLevel::Warning, "Warning"},
            {SeverityLevel::Error,   "Error"},
            {SeverityLevel::Fatal,   "Fatal"}
        };
        
        struct Entry
        {
            std::chrono::time_point<std::chrono::system_clock> timestamp;
            SeverityLevel severity_level;
            std::string catgegory;
            std::filesystem::path path;
            int line;
            int column;
            std::string function;
            std::string message;
        };

        std::vector<Entry> records;


        /*!
            Get the current time as a text string.

            @param time_zone For which time zone: UTC or local time?
            @param OnlyDate  Return only year, month and day in the string.

            @return A text string of the requested current time.
        */
        std::string timestamp (TimeZone time_zone, bool OnlyDate = false)
        {
            // [TODO, 2022-03-05] I wanted to use (only) "chrono" and "format" (C++20), but MSVC 2019 doesn't support "format" (yet?).

            std::tm tm = {};
            std::stringstream timestamp;
            
            // With some workarounds to get milliseconds, which are not part of timeinfo/strftime; based on <https://stackoverflow.com/a/68132528/17589673>.
            if (time_zone == TimeZone::UTC)
            {
                auto now_utc_sys = std::chrono::utc_clock::to_sys(std::chrono::utc_clock::now());
                std::time_t t = std::chrono::system_clock::to_time_t(now_utc_sys);
                
                auto now_truncated = std::chrono::system_clock::from_time_t(t);
                auto milliseconds = (now_utc_sys - now_truncated).count();
                
                tm = *std::gmtime(&t);
                
                OnlyDate ? timestamp << std::put_time(&tm, "%Y-%m-%d")
                         : timestamp << std::put_time(&tm, "%Y-%m-%d %H:%M:%S.") << std::to_string(milliseconds).substr(0, 3) << std::put_time(&tm, " +0000");
                            // Using hardcoded '+0000' instead of %z for UTC, because %z always gives the offset of the local time(zone) from UTC, even when querying UTC time).
            }
            else
            {
                auto now = std::chrono::system_clock::now();
                std::time_t t = std::chrono::system_clock::to_time_t(now);
                
                auto now_truncated = std::chrono::system_clock::from_time_t(t);
                auto milliseconds = (now - now_truncated).count();

                tm = *std::localtime(&t);
                
                OnlyDate ? timestamp << std::put_time(&tm, "%Y-%m-%d")
                         : timestamp << std::put_time(&tm, "%Y-%m-%d %H:%M:%S.") << std::to_string(milliseconds).substr(0, 3) << std::put_time(&tm, " %z");
            }

            return timestamp.str();
        }


        /*!
            Get the current time as a std::chrono::time_point.
        */
        std::chrono::time_point<std::chrono::system_clock> timepoint (TimeZone time_zone)
        {
            std::chrono::time_point<std::chrono::system_clock> tp;

            if (time_zone == TimeZone::UTC)
            {
                tp = std::chrono::utc_clock::to_sys(std::chrono::utc_clock::now());
            }
            else
            {
                tp = std::chrono::system_clock::now();
            }

            return tp;
        }


        /*!
            Save the records of log entries to a CSV text file.

            Note that the column (names) should reflect the names of the struct members of 'Entry'!
        */
        void saveAsCSV (std::filesystem::path path, OpenMode om = OpenMode::Append)
        {
            std::ofstream csv_file;
            
            if (om == OpenMode::Truncate || !std::filesystem::exists(path))
            {
                // The first line is recommended to be the CSV header (with the column names).
                // If it's an existing file that is being truncated, or if it's a brand-new file we need to generate that.
                csv_file.open(path.string(), std::ios_base::trunc | std::ios::out | std::ios::binary);
                csv_file << "timestamp;severity_level;category;path;line;column;function;message" << std::endl;
            }
            else
            {
                csv_file.open(path.string(), std::ios_base::app | std::ios::out | std::ios::binary);
            }
                   
            // Append content lines:
            for (auto& i : records)
            {
                // Convert time_point again to a printable string (only as local time!).
                std::time_t t = std::chrono::system_clock::to_time_t(i.timestamp);
                
                auto now_truncated = std::chrono::system_clock::from_time_t(t);
                auto milliseconds = (i.timestamp - now_truncated).count();

                std::tm tm = {};
                tm = *std::localtime(&t);
                
                std::stringstream timestamp;
                timestamp << std::put_time(&tm, "%Y-%m-%d %H:%M:%S.") << std::to_string(milliseconds).substr(0, 3) << std::put_time(&tm, " %z");
                
                // Mind the order: Header and content must match!
                csv_file << timestamp.str() << ";" \
                         << SeverityLevelString[i.severity_level] << ";" \
                         << i.catgegory << ";" \
                         << i.path.string() << ";" \
                         << i.line << ";" \
                         << i.column << ";" \
                         << i.function << ";" \
                         << i.message << std::endl;
            }

            if (csv_file.is_open())
            {
                csv_file.close();
                os << "Log saved in CSV format to file " << path.string() << std::endl;
            }
                
        }
};
    
} // namespace
 
#endif // include guard