![TNOT Logo](./src/assets/TNOT-Logo-1.png "Logo of The Nifty Oddity Toolkit (TNOT)")

A collection of C++ source code with some convenience classes, free helper functions and other auxiliary parts and pieces that may come handy at times.

----------------------------------------------------------------------------------------------------

# Objectives

1. Each component of this unmbrella project should stand on its own and be usable indivually, so that
   every generated artifact, like a library for example, should be independent from other components of _TNOT_.
   After all, this is supposed to be a set of tools and utilities, not a framework.

2. While having this spirit of independence in mind, the build process itself will still make use of
   a hierarchic structure: The parent project (respectively the top CMakeLists.txt) defines several
   basic configuration settings (e.g. for the platform or compiler), so that this doesn't need to be
   repeated/repeatly changed in the _n_ subprojects.
   
3. If possible (and if practical in use!), apply methods that are platform-agnostic and/or standard-conforming.

----------------------------------------------------------------------------------------------------

# Contents

**CastConvert**
:   Free utility functions for casting and converting data types.

**CLI**
:   Helper classes and free functions for the Command Line Interface [on Windows].

**InitFile**
:   A class for reading and writing text-based configuration files (aka INI-files).

**KeyboardLED**
:   A class for making use of a keyboard's standard status LEDs.

**Log**
:   A class to help with logging.

**Stopwatch**
:   A class for measuring time spans.

**TextUtilities**
:   Helper functions for processing and modifying text (strings).

**FileUtilities**
:   Helper functions for working with files.

**Win32GUI**
:   Free helper functions for the Win32 API and GUI

----------------------------------------------------------------------------------------------------

# Build

## Prerequisites

_TNOT_ uses some features of **C++20**, so make sure to use a compiler version that supports this
version of the standard and that your CMakeLists.txt is set up accordingly (for example via `set (CMAKE_CXX_STANDARD 20)`).

_TNOT_ is currently primarily created on and for **Microsoft Windows**, but the idea is to use
platform-agnostic solutions whenever possible/practical.

Tools you'll need (some are optional):

- [Git](https://git-scm.com/), a distributed version control system
- [CMake](http://www.cmake.org), a build-system generator
- [Microsoft Visual Studio](https://visualstudio.microsoft.com/) ([Community Edition](https://visualstudio.microsoft.com/de/vs/community/)), or a similar C++ build environment
- [Doxygen](https://www.doxygen.nl/) (optional), a tool for generating documentation from the annotated C++ source code


## Get the source code from the repository

Clone this repository:

    C:\> git clone https://saoe@bitbucket.org/saoe/theniftyodditytoolkit.git .\TheNiftyOddityToolkit
    
## Either use CMake presets...

The project is now using the new CMake feature of [Presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)
(fully available since CMake 3.20) for easier handling of the configuration/building/installation steps;
this is the recommended way.

To get an overview of the available presets, run this:

    C:\TheNiftyOddityToolkit\> cd src
    C:\TheNiftyOddityToolkit\src> cmake --list-presets all

Then, you can use one of those available presets to configure, build, and install the project:  
(Hint: If you can't decide which option to choose, try the _"default"_ presets.)

    C:\TheNiftyOddityToolkit\src> cmake --preset <name_of_a_CONFIG_preset>
    
    C:\TheNiftyOddityToolkit\src> cmake --build --preset <name_of_a_BUILD_preset>
    
    C:\TheNiftyOddityToolkit\src> cmake --install <path_of_the_build_directory> [--prefix <path>]

If you want to override the installation destination path that was already configured,
you can provide a different path here with _--prefix_.

## ... or call CMake directly

This information may be useful if you (for some reason) won't use the CMake presets, but call CMake directly;
not sure for how long I will keep this info here up-to-date though...

The recommended way is to use the CMake presets!

But if you want to go down this road, you probably know what you're doing and can also look into the
above mentioned files and derive the required settings.

### 1. Configure

If optional settings are not provided, default values (specified by either CMake or the project) will be applied.

Things like _GeneratorName_, _ToolsetName_ etc., only accept specfic values; please consult the CMake documentation for details.

Example:

    cmake -D GenerateDocumentation=On -D CMAKE_INSTALL_PREFIX=C:\devel\shared -S src -B _build

Remarks:

- `GenerateDocumentation` controls whether the Doxygen documentation should generated (`On`) or not (`Off`).
- `CMAKE_INSTALL_PREFIX` defines the base path in which the files and folder should be installed
  (when the _Install_ target is selected in the build step).
- `S` tells CMake in which directory it should look for the sources.
- `B` tells CMake in which directory it should output the build artifacts (note: in-source builds are not supported by this project).

After the configuration is done, you can continue to use CMake for building (next step),
or you can open the generated files with other tools (e.g. the `*.sln` files with the _Visual Studio IDE_).

### 2. Build (and Install)

Example:

    cmake --build _build --target INSTALL --config Release --parallel

Remarks:

- Use the target _ALL_BUILD_ (for Visual Studio; or simply omit any target) to build all parts of the project (which have a dependency to `all`).
- The target _Install_ depends on _ALL_BUILD_, and thus all projects that depend on the `all` target will automatically also be built.

----------------------------------------------------------------------------------------------------

# Tests

See the [ReadMe in 'tests'](src/tests/ReadMe.md) for details on that.

----------------------------------------------------------------------------------------------------

# Usage

## Example: How to integrate this package in your own CMake project

1. First off, the assumption for this example is that TNOT is already installed, as described in the previous section.

2. Then give _your_ CMake-based project a hint where to look for the package, by providing the base path via
   `-DCMAKE_PREFIX_PATH="C:\Path\To\TheNiftyOddityToolkit"` (on the command-line, or via CMake presets...)

3. Next, in your project's CMakeLists.txt, find the package [and the component(s) from it that you want to use]:

        find_package (TheNiftyOddityToolkit
            CONFIG                          # Use Package Config mode for setup.
            REQUIRED                        # If the package is mandatory for your project.
            COMPONENTS                      # List of the package's components that will be used.
                CLI
        )

4. Finally, link it to your target (although, in this case, it's technically not really "linking",
   since it's a header-only library, but CMake knows how to deal with it anyways).

        project ("YourApplication")

        # ...

        target_link_libraries (${PROJECT_NAME}
            PRIVATE
                TheNiftyOddityToolkit::CLI
        )

    That is all; things like the include path for the header files are provided by the package
    configuration and will be inherited to your target automatically.

5. This is how it then would look like on the user's side of things:

        // YourApplication.cpp

        #include <iostream>
        #include <TheNiftyOddityToolkit/CLI/Options.hpp>

        namespace tnotco = NiftyOddity::Toolkit::CLI::Options;

        int main (int argc, char* argv[])
        {	
            std::string switch_argument_str;

            for (int i = 0; i < argc; i++)
            {
                // Handle a simple switch (e.g. "C:\>example.exe /sw1" or "C:\>example.exe -sw2").
                if      (tnotco::isSwitch(argv[i], "sw1")) { printf("Switch 1\n"); }
                else if (tnotco::isSwitch(argv[i], "sw2")) { printf("Switch 2\n"); }
                // [...]
            }
            return 0;
        }

----------------------------------------------------------------------------------------------------

# Copyright & License

Copyright © 2021-2022 Sascha Offe <so@saoe.net>  
SPDX-License-Identifier: MIT-0  
(For details, see accompanying file LICENSE.txt)