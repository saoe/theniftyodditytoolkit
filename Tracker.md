# Keeping track of what to do next, but also ideas for and notes for this project

----------------------------------------------------------------------------------------------------

Issue: version.h is still not generated after a fresh build; need to build target VersionHeader first.

2022-03-13 (so): Not new, but I hoped to had that fixed by some CMake magic...

Only stands out when one throws away the whole build directory and (after CMake configuration)
directly want to build target X: Can't find/include version.h -> run target VersionHeader -> OK
=> so, not critical, but also not nice...

----------------------------------------------------------------------------------------------------

TNOT::AutoUpdater

- Singleton class?

Components:
- program.exe     - the actual application that gets updated.
- update_help.dll - Misc. helper functions for program.exe and updater.exe.
- updater.exe     - Little helper executable that takes care of the actual update and clean-up process.
- settings{.ini|.json|...}

Workflow:
1. User starts program.exe

2. program.exe checks[1] for a new version[2]

    [1] By some functionality of update_help.dll
    [2]

3. New version was detected; act as defined in settings:
   Check... on each start | every <time interval> | manually | never
   Notify only
   Download only
   Download & Install
   /silent
   Download Save Location

4.1 Download only
4.2 Download & Install

[!] C++ itself doesn't know how to talk to the network.
    either use (small) 3rd party lib., or maybe some platform-specific stuff:
    Win32: WinHTTPServices, WinINet, URLDownloadToFile(!?)
    
    https://docs.microsoft.com/en-us/windows/win32/wininet/wininet-vs-winhttp
    "With a few exceptions, WinINet is a superset of WinHTTP. When selecting between the two, you should use WinINet,
    unless you plan to run within a service or service-like process that requires impersonation and session isolation."
    https://docs.microsoft.com/en-us/windows/win32/winhttp/porting-wininet-applications-to-winhttp
    https://docs.microsoft.com/en-us/windows/win32/wininet/http-sessions?redirectedfrom=MSDN#downloading-resources-from-the-www
    
    ---
    Package your program as an MSI file (Windows installer). Then download your new msi file and launch it. Windows installer takes care of updating your program and you can author the installer to relaunch your app.
    Take a look at http://wixtoolset.org/ for how to build your installer.


5. program.exe terminates itself (to allow access to itself?);
   (?) but starts updater.exe first in a separate thread.

6. updater.exe takes over: Starts setup.exe of the newly downloaded version and afterwards
   cleans up (deletes old/unused files).

or:
Install a service (in Windows, as Local System?) that takes care...

----------------------------------------------------------------------------------------------------

TNOT::FilesystemObserver

2022-03-06: Brainstorming phase, writing down ideas.
2022-03-10: More ideas.
2022-03-22: More brainstorming; bookmarked https://solarianprogrammer.com/2019/01/13/cpp-17-filesystem-write-file-watcher-monitor/
and https://docs.microsoft.com/en-us/dotnet/api/system.io.filesystemwatcher?view=net-6.0 (C#/.NET)
for inspiration.

Short desc.: "A class that implements/provides a lightweight filesystem observer (to monitor 
              some kinds of modifications to directories and files)"

First practical use case/origin: Project 'Jukebox' (monitor music collection/media library).

As always: Focus on Windows/NTFS

Input:
- List of directories that should be observed
- Recursion depth (each? indivdual?)

Output/Result/Intended Use:
- To signal the client what has changed;
  e.g. {Directory|File} {added|removed|updated/changed/modified}
- How to 'signal'? Send some kind of event notification (to subscribers) -> Sounds like its own
  subsystem... Or trigger/call/invoke some action (= function and/or lambda)?

Implementation:
- As a background thread of the client application?
- Header only or header/lib (dynamic and/or static)???

Member functions:
- add(
     Overlaod #1: std:string path // singular dir or file
     Overlaod #2: std::filesystem::... path // singular dir or file
     Overlaod #3: std::vector<std:string> paths // multiple dirs or files
     Overlaod #4: std::vector<std::filesystem::...> paths // multiple dirs or files
     )
- remove (/* mirror add(...) */)

___
EOF